import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import enTranslation from './en';
import ruTranslation from './ru';

const resources = {
  en: {
    translation: enTranslation,
  },
  ru: {
    translation: ruTranslation,
  },
};

i18n.use(initReactI18next).init({
  resources,
  compatibilityJSON: 'v3',
  lng: 'en',
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
