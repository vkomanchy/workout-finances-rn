import {
  Box,
  Divider,
  Flex,
  HStack,
  Heading,
  Icon,
  IconButton,
  Stack,
  Text,
  Popover,
  Button,
} from 'native-base';

import { useTranslation } from 'react-i18next';

import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import { WorkoutTypeItemProps } from '../types';

const WorkoutTypeItem: React.FC<WorkoutTypeItemProps> = ({
  data,
  onDeleteWorkoutType,
  onEditWorkoutType,
}) => {
  const { t } = useTranslation();

  const { name, incomeToCashier, incomeToTrainer, id } = data;

  const deleteModalHeading = t('workoutTypeItem.deleteModal.title', { name });

  const deleteModalBody = (
    <Text>{t('workoutTypeItem.deleteModal.body', { name })}</Text>
  );

  const handleDeleteWorkoutType = () => {
    onDeleteWorkoutType(id);
  };

  const handleEditWorkoutType = () => {
    onEditWorkoutType(data);
  };

  return (
    <Box alignItems='center' my={2}>
      <Box
        maxW='80'
        rounded='lg'
        overflow='hidden'
        borderColor='coolGray.200'
        borderWidth='1'
        _dark={{
          borderColor: 'coolGray.600',
          backgroundColor: 'gray.700',
        }}
        _web={{
          shadow: 2,
          borderWidth: 0,
        }}
        _light={{
          backgroundColor: 'gray.50',
        }}
      >
        <Stack p={4} space={2}>
          <HStack
            paddingTop='10px'
            height='20px'
            alignItems='flex-end'
            justifyContent='flex-end'
          >
            <Box mx={2}>
              <Popover
                trigger={(triggerProps) => (
                  <IconButton
                    icon={<Icon as={AntDesignIcon} name='delete' />}
                    borderRadius='full'
                    _icon={{
                      color: 'red.500',
                      size: 'xs',
                    }}
                    {...triggerProps}
                  />
                )}
              >
                <Popover.Content
                  accessibilityLabel={t`workoutTypeItem.deleteModal.deleteLabel`}
                  w='56'
                >
                  <Popover.CloseButton />

                  <Popover.Header>{deleteModalHeading}</Popover.Header>

                  <Popover.Body>{deleteModalBody}</Popover.Body>

                  <Popover.Footer justifyContent='flex-end'>
                    <Button.Group space={2}>
                      <Button
                        colorScheme='danger'
                        onPress={handleDeleteWorkoutType}
                      >
                        {t('workoutTypeItem.deleteModal.deleteButton')}
                      </Button>
                    </Button.Group>
                  </Popover.Footer>
                </Popover.Content>
              </Popover>
            </Box>

            <Box>
              <IconButton
                icon={<Icon as={AntDesignIcon} name='edit' />}
                borderRadius='full'
                _icon={{
                  size: 'xs',
                }}
                onPress={handleEditWorkoutType}
              />
            </Box>
          </HStack>

          <HStack space={2} alignItems='center' justifyContent='center'>
            <Heading size='md'>{name}</Heading>
          </HStack>

          <Flex
            alignItems='center'
            direction='row'
            justifyContent='space-between'
          >
            <Text
              fontSize='xs'
              _light={{
                color: 'violet.500',
              }}
              _dark={{
                color: 'violet.400',
              }}
              fontWeight='500'
            >
              {t('workoutTypeItem.incomeToOrganization', { incomeToCashier })}
            </Text>

            <Divider mx={2} thickness='2' orientation='vertical' />

            <Text
              fontSize='xs'
              _light={{
                color: 'green.500',
              }}
              _dark={{
                color: 'green.400',
              }}
              fontWeight='500'
            >
              {t('workoutTypeItem.incomeToTrainer', { incomeToTrainer })}
            </Text>
          </Flex>
        </Stack>
      </Box>
    </Box>
  );
};

export default WorkoutTypeItem;
