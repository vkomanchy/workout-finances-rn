import { expect, it, describe } from 'vitest';
import { filterWorkoutsTypes } from '../filterWorkoutsTypes';
import { WorkoutType } from '../../../calculations/types';

describe('Check filterWorkoutsTypes function', () => {
  it('Check that item with id "123456" not in list after filtering', () => {
    const workoutTypesList = [
      {
        id: '123456',
      },
      {
        id: 'ajkdwjecv',
      },
    ];

    const idForRemoving = '123456';

    const result = filterWorkoutsTypes(
      idForRemoving,
      workoutTypesList as WorkoutType[],
    );

    expect(result).toHaveLength(1);
    expect(result[0].id).not.toBe(idForRemoving);
  });

  it('Check that list without id for removing will be same after function execution', () => {
    const workoutTypesList = [
      {
        id: '123456',
      },
      {
        id: 'ajkdwjecv',
      },
    ];

    const idForRemoving = 'test';

    const result = filterWorkoutsTypes(
      idForRemoving,
      workoutTypesList as WorkoutType[],
    );

    expect(result).toHaveLength(2);

    expect(result).toMatchObject(workoutTypesList);
  });
});
