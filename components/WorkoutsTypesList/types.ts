import { WorkoutType } from '../../calculations/types';

export interface WorkoutsTypesListProps {
  workoutsTypes: WorkoutType[];
  onRefresh: () => void;
  onEdit: (data: WorkoutType) => void;
}

export interface WorkoutTypeItemProps {
  data: WorkoutType;
  onDeleteWorkoutType: (id: string) => void;
  onEditWorkoutType: (data: WorkoutType) => void;
}
