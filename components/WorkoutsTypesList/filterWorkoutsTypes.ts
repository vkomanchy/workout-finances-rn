import * as R from 'ramda';
import { WorkoutType } from '../../calculations/types';

const filterByNotEqualId = (
  idForRemoving: string,
  workoutTypesList: WorkoutType[],
) => R.filter(({ id }) => id !== idForRemoving, workoutTypesList);

export const filterWorkoutsTypes = filterByNotEqualId;
