import { ScrollView } from 'native-base';
import WorkoutTypeItem from './WorkoutTypeItem';
import { WorkoutsTypesListProps } from './types';
import { filterWorkoutsTypes } from './filterWorkoutsTypes';
import { WORKOUT_TYPES_STORAGE_KEY } from '../../constants';
import { storeObjectData } from '../../database';
import { fetchWorkoutTypes } from '../../pagesUtils/fetchWorkoutTypes';

const WorkoutsTypesList: React.FC<WorkoutsTypesListProps> = ({
  workoutsTypes,
  onRefresh,
  onEdit,
}) => {
  const handleDeleteWorkoutType = async (idForRemoving: string) => {
    const currentWorkoutTypes = await fetchWorkoutTypes();
    const newWorkoutsTypes = filterWorkoutsTypes(
      idForRemoving,
      currentWorkoutTypes,
    );

    await storeObjectData(WORKOUT_TYPES_STORAGE_KEY, newWorkoutsTypes);

    onRefresh();
  };

  return (
    <ScrollView>
      {workoutsTypes.map((item) => (
        <WorkoutTypeItem
          key={item.id}
          data={item}
          onDeleteWorkoutType={handleDeleteWorkoutType}
          onEditWorkoutType={onEdit}
        />
      ))}
    </ScrollView>
  );
};

export default WorkoutsTypesList;
