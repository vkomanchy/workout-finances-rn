import { HStack, Text, Heading, IconButton, Icon, VStack } from 'native-base';
import { useTranslation } from 'react-i18next';

import { useMemo } from 'react';
import { DateTime } from 'luxon';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import { PeriodPickerProps } from './types';

const PeriodPicker: React.FC<PeriodPickerProps> = ({
  currentPeriod: { startDate, endDate },
  style,
  periodsAvailability,
  onChangePeriod,
}) => {
  const { t } = useTranslation();

  const CurrentPeriodText = style === 'heading' ? Heading : Text;

  const handlePrevClick = () => {
    onChangePeriod('minus');
  };

  const handleNextClick = () => {
    onChangePeriod('plus');
  };

  const formattedStartDate = useMemo(
    () => DateTime.fromMillis(startDate).toLocaleString(),
    [startDate],
  );

  const formattedEndDate = useMemo(
    () => DateTime.fromMillis(endDate).toLocaleString(),
    [endDate],
  );

  if (style === 'inline')
    return (
      <HStack justifyContent='space-between' alignItems='center'>
        <CurrentPeriodText fontSize='sm'>
          {formattedStartDate}-{formattedEndDate}
        </CurrentPeriodText>

        <HStack minW='50'>
          <IconButton
            icon={<Icon as={AntDesignIcon} name='caretleft' />}
            isDisabled={!periodsAvailability.isMinusAvailable}
            borderRadius='full'
            _icon={{
              size: 'xs',
            }}
            onPress={handlePrevClick}
          />

          <IconButton
            icon={<Icon as={AntDesignIcon} name='caretright' />}
            isDisabled={!periodsAvailability.isPlusAvailable}
            borderRadius='full'
            _icon={{
              size: 'xs',
            }}
            onPress={handleNextClick}
          />
        </HStack>
      </HStack>
    );

  return (
    <HStack justifyContent='space-between' alignItems='center'>
      <VStack>
        <CurrentPeriodText size='sm'>{t`periodPicker.currentPeriod`}</CurrentPeriodText>

        <CurrentPeriodText size='sm' minH={5}>
          {formattedStartDate}-{formattedEndDate}
        </CurrentPeriodText>
      </VStack>

      <HStack minW='50'>
        <IconButton
          icon={<Icon as={AntDesignIcon} name='caretleft' />}
          isDisabled={!periodsAvailability.isMinusAvailable}
          borderRadius='full'
          _icon={{
            size: 'xs',
          }}
          onPress={handlePrevClick}
        />

        <IconButton
          icon={<Icon as={AntDesignIcon} name='caretright' />}
          isDisabled={!periodsAvailability.isPlusAvailable}
          borderRadius='full'
          _icon={{
            size: 'xs',
          }}
          onPress={handleNextClick}
        />
      </HStack>
    </HStack>
  );
};

export default PeriodPicker;
