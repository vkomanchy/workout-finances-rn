import { CalcPeriod } from '../../types/settings';
import { Period } from '../../calculations/types';

export type ChangePeriodValues = 'plus' | 'minus';

export type PeriodPickerStyles = 'heading' | 'inline';

export interface PeriodsAvailability {
  isPlusAvailable: boolean;
  isMinusAvailable: boolean;
}

export interface PeriodPickerProps {
  currentPeriod: Period;
  style: PeriodPickerStyles;
  periodType: CalcPeriod;

  periodsAvailability: PeriodsAvailability;

  onChangePeriod: (value: ChangePeriodValues) => void;
}
