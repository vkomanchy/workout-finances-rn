import {
  WorkoutRecordWithExpandedWorkoutType,
  WorkoutRecordWithWorkoutTypeId,
  WorkoutType,
} from '../../calculations/types';

export interface WorkoutModalProps {
  isOpen: boolean;
  onClose: () => void;
  onSubmit: (data: WorkoutRecordWithWorkoutTypeId) => void;
  workoutTypes: WorkoutType[];
  editData: WorkoutRecordWithExpandedWorkoutType;
}

export interface WorkoutRecordState {
  workoutType: string | null;
  date: Date;
  peopleCount: string;
}
