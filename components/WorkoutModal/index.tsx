import {
  Modal,
  ScrollView,
  Button,
  FormControl,
  Stack,
  Select,
  CheckIcon,
  Text,
  HStack,
  VStack,
  Input,
} from 'native-base';
import { useReducer, useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { DateTime } from 'luxon';
import { customAlphabet } from 'nanoid/non-secure';
import { DateTimePickerAndroid } from '@react-native-community/datetimepicker';

import { WorkoutModalProps } from './types';
import { initialState, reducer } from './reducer/reducer';
import {
  setDate,
  setEditData,
  setPeopleCount,
  setWorkoutType,
} from './reducer/actions';
import { ValidationResult } from '../WorkoutTypeModal/types';
import { validateWorkoutRecord } from './validation';

const nanoid = customAlphabet('abcdefghijklmnopqrstuvwxyz0123456789', 10);

const WorkoutModal: React.FC<WorkoutModalProps> = ({
  isOpen,
  workoutTypes,
  editData,
  onClose,
  onSubmit,
}) => {
  const { t } = useTranslation();

  const [state, dispatch] = useReducer(reducer, initialState);
  const [errors, setErrors] = useState<ValidationResult>({});

  const { workoutType, date, peopleCount } = state;

  const handleChangeDateTime = (_, selectedDate) => {
    dispatch(setDate(selectedDate));
  };

  const handleChangePeopleCount = (text = '') => {
    dispatch(setPeopleCount(text));
  };

  const showMode = (currentMode) => {
    DateTimePickerAndroid.open({
      value: date,
      onChange: handleChangeDateTime,
      mode: currentMode,
      is24Hour: true,
    });
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const showTimepicker = () => {
    showMode('time');
  };

  const validateState = (): boolean => {
    const newErrors = validateWorkoutRecord(state);

    setErrors(newErrors);

    return Object.keys(newErrors).length === 0;
  };

  const handleSubmit = () => {
    const validationResult = validateState();

    if (validationResult) {
      onSubmit({
        workoutType,
        date: date.valueOf(),
        peopleCount: Number(peopleCount),
        id: editData ? editData.id : nanoid(),
      });
    }
  };

  useEffect(() => {
    if (editData) {
      dispatch(
        setEditData({
          date: new Date(editData.date),
          peopleCount: editData.peopleCount.toString(),
          workoutType: editData.workoutType?.id,
        }),
      );
    }
  }, [editData]);

  const isEdit = !!editData;

  return (
    <Modal isOpen={isOpen} onClose={onClose} size='xl'>
      <Modal.Content maxH='450'>
        <Modal.CloseButton />
        <Modal.Header>
          {t(isEdit ? 'workoutModal.title.edit' : 'workoutModal.title.create')}
        </Modal.Header>

        <Modal.Body>
          <ScrollView>
            <Stack mx={2}>
              <FormControl.Label>{t`workoutModal.fieldsLabels.workoutType`}</FormControl.Label>

              <Select
                selectedValue={workoutType}
                minWidth='200'
                placeholder={t`workoutModal.fieldsPlaceholders.workoutType`}
                accessibilityLabel={t`workoutModal.fieldsPlaceholders.workoutType`}
                _selectedItem={{
                  bg: 'teal.600',
                  endIcon: <CheckIcon size='5' />,
                }}
                isFocusVisible={false}
                mt={1}
                onValueChange={(itemValue) => {
                  dispatch(setWorkoutType(itemValue));
                }}
              >
                {workoutTypes.map(({ id, name }) => (
                  <Select.Item key={id} label={name} value={id} />
                ))}
              </Select>
            </Stack>

            <Stack mx={2}>
              <FormControl.Label>{t`workoutModal.fieldsLabels.date`}</FormControl.Label>

              <HStack justifyContent='space-between' alignItems='center'>
                <VStack>
                  <Text>
                    {DateTime.fromJSDate(date).toLocaleString(
                      DateTime.DATETIME_MED,
                    )}
                  </Text>
                </VStack>

                <Button
                  onPress={showDatepicker}
                  w='70px'
                  h='45px'
                  fontSize='8px'
                  size='xs'
                >
                  {t`workoutModal.fieldsLabels.chooseDate`}
                </Button>

                <Button
                  onPress={showTimepicker}
                  w='70px'
                  h='45px'
                  fontSize='8px'
                  size='xs'
                >
                  {t`workoutModal.fieldsLabels.chooseTime`}
                </Button>
              </HStack>
            </Stack>

            <FormControl isRequired isInvalid={!!errors.peopleCount}>
              <Stack mx='2'>
                <FormControl.Label>{t`workoutModal.fieldsLabels.peopleCount`}</FormControl.Label>

                <Input
                  value={`${peopleCount}`}
                  onChangeText={handleChangePeopleCount}
                  autoCapitalize='none'
                />

                {errors.peopleCount && (
                  <FormControl.ErrorMessage>
                    {errors.peopleCount.message}
                  </FormControl.ErrorMessage>
                )}
              </Stack>
            </FormControl>
          </ScrollView>
        </Modal.Body>

        <Modal.Footer>
          <Button.Group space={2}>
            <Button variant='ghost' colorScheme='blueGray' onPress={onClose}>
              {t`workoutModal.buttons.cancel`}
            </Button>

            <Button onPress={handleSubmit}>
              {t`workoutModal.buttons.save`}
            </Button>
          </Button.Group>
        </Modal.Footer>
      </Modal.Content>
    </Modal>
  );
};

export default WorkoutModal;
