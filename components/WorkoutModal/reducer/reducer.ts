import { actionsTypes } from './actions';
import { ActionType } from '../../../types/settings';
import { WorkoutRecordState } from '../types';

export const initialState: WorkoutRecordState = {
  workoutType: null,
  date: new Date(),
  peopleCount: '',
};

export const reducer = (
  state: WorkoutRecordState,
  action: ActionType<WorkoutRecordState>,
) => {
  switch (action.type) {
    case actionsTypes.SET_WORKOUT_TYPE: {
      return {
        ...state,
        ...action.payload,
      };
    }

    case actionsTypes.SET_DATE: {
      return {
        ...state,
        ...action.payload,
      };
    }

    case actionsTypes.SET_PEOPLE_COUNT: {
      return {
        ...state,
        ...action.payload,
      };
    }

    case actionsTypes.SET_EDIT_DATA: {
      return {
        ...state,
        ...action.payload,
      };
    }

    default:
      return state;
  }
};
