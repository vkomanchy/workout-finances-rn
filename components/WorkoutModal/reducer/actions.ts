import { WorkoutRecordState } from '../types';

export const actionsTypes = {
  SET_WORKOUT_TYPE: 'SET_WORKOUT_TYPE',
  SET_DATE: 'SET_DATE',
  SET_PEOPLE_COUNT: 'SET_PEOPLE_COUNT',
  SET_EDIT_DATA: 'SET_EDIT_DATA',
};

export const setWorkoutType = (workoutType: string) => ({
  type: actionsTypes.SET_WORKOUT_TYPE,
  payload: {
    workoutType,
  },
});

export const setDate = (date: Date) => ({
  type: actionsTypes.SET_DATE,
  payload: {
    date,
  },
});

export const setPeopleCount = (peopleCount: string) => ({
  type: actionsTypes.SET_PEOPLE_COUNT,
  payload: {
    peopleCount,
  },
});

export const setEditData = (data: Partial<WorkoutRecordState>) => ({
  type: actionsTypes.SET_EDIT_DATA,
  payload: data,
});
