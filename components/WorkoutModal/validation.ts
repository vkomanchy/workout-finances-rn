import { ValidationResult } from '../WorkoutTypeModal/types';
import { validateNumber } from '../WorkoutTypeModal/validation';
import { initialState } from './reducer/reducer';

import i18n from '../../i18n';

// TODO: add unit tests
export const validateWorkoutRecord = (
  state: typeof initialState,
): ValidationResult => {
  const validationResult: ValidationResult = {};

  if (!state.workoutType || state.workoutType.length === 0) {
    validationResult.workoutType = {
      message: i18n.t`workoutModal.validationMessages.workoutType`,
    };
  }

  if (!state.peopleCount) {
    validationResult.peopleCount = {
      message: i18n.t`workoutModal.validationMessages.peopleCount`,
    };
  } else {
    if (Number.isNaN(Number(state.peopleCount))) {
      validationResult.peopleCount = {
        message: i18n.t`workoutModal.validationMessages.peopleCountNotNumber`,
      };
    }

    if (
      !validateNumber(Number(state.peopleCount)) ||
      Number(state.peopleCount) <= 0
    ) {
      validationResult.peopleCount = {
        message: i18n.t`workoutModal.validationMessages.shouldBeGreaterThanZero`,
      };
    }
  }

  return validationResult;
};
