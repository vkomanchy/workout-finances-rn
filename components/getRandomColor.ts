import { Appearance } from 'react-native';

import { Theme } from '../store/types';

const colorsByTheme = {
  dark: [
    'green.300',
    'lime.300',
    'yellow.300',
    'amber.300',
    'red.400',
    'blueGray.400',
  ],
  light: [
    'green.300',
    'lime.300',
    'yellow.300',
    'amber.300',
    'red.400',
    'blueGray.400',
  ],
};

export const getRandomColor = (theme: Theme) => {
  let currentTheme = theme;

  if (theme === 'os') {
    currentTheme = Appearance.getColorScheme();
  }

  const colors = colorsByTheme[currentTheme];

  return colors[Math.floor(Math.random() * colors.length)];
};
