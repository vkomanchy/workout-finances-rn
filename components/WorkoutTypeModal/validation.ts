import { initialState } from './reducer/reducer';
import { ValidationResult } from './types';
import i18n from '../../i18n';

export const validateNumber = (value: number) => {
  if (value < 0) return false;

  return true;
};

// TODO: add unit tests
export const validateWorkoutType = (
  state: typeof initialState,
): ValidationResult => {
  const validationResult: ValidationResult = {};

  if (!state.name || state.name.length === 0) {
    validationResult.name = {
      message: i18n.t`workoutTypeModal.validationMessages.name`,
    };
  }

  if (!state.incomeToCashier) {
    validationResult.incomeToCashier = {
      message: i18n.t`workoutTypeModal.validationMessages.incomeToOrganization`,
    };
  } else if (Number.isNaN(Number(state.incomeToCashier))) {
    validationResult.incomeToCashier = {
      message: i18n.t`workoutTypeModal.validationMessages.incomeToOrganization`,
    };
  }

  if (!state.incomeToTrainer) {
    validationResult.incomeToTrainer = {
      message: i18n.t`workoutTypeModal.validationMessages.incomeToTrainer`,
    };
  } else {
    if (Number.isNaN(Number(state.incomeToTrainer))) {
      validationResult.incomeToTrainer = {
        message: i18n.t`workoutTypeModal.validationMessages.incomeToTrainer`,
      };
    }

    if (!validateNumber(Number(state.incomeToTrainer))) {
      validationResult.incomeToTrainer = {
        message: i18n.t`workoutTypeModal.validationMessages.shouldBeGreaterThanZero`,
      };
    }
  }

  return validationResult;
};
