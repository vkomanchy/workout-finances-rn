import { WorkoutType } from '../../calculations/types';

export interface WorkoutTypeModalProps {
  isOpen: boolean;
  editData: WorkoutType | null;

  onClose: () => void;
  onSubmit: (data: WorkoutType) => void;
}

export interface WorkoutTypeState extends Pick<WorkoutType, 'name'> {
  id?: string;

  incomeToCashier: string;
  incomeToTrainer: string;
}

export interface ValidationResult {
  [key: string]: ValidationError;
}

export interface ValidationError {
  message: string;
}
