import {
  Modal,
  ScrollView,
  Button,
  FormControl,
  Stack,
  Input,
  WarningOutlineIcon,
} from 'native-base';

import { useTranslation } from 'react-i18next';
import { useEffect, useReducer, useState } from 'react';

import { customAlphabet } from 'nanoid/non-secure';

import { ValidationResult, WorkoutTypeModalProps } from './types';
import { initialState, reducer } from './reducer/reducer';
import {
  setEditData,
  setIncomeToCashier,
  setIncomeToTrainer,
  setName,
} from './reducer/actions';
import { WorkoutType } from '../../calculations/types';
import { validateWorkoutType } from './validation';

const nanoid = customAlphabet('abcdefghijklmnopqrstuvwxyz0123456789', 10);

const WorkoutTypeModal: React.FC<WorkoutTypeModalProps> = ({
  isOpen,
  editData,
  onClose,
  onSubmit,
}) => {
  const { t } = useTranslation();

  const [state, dispatch] = useReducer(reducer, initialState);
  const [errors, setErrors] = useState<ValidationResult>({});

  const handleChangeName = (text = '') => {
    dispatch(setName(text));
  };

  const handleChangeIncomeToCashier = (text = '') => {
    dispatch(setIncomeToCashier(text));
  };

  const handleChangeIncomeToTrainer = (text = '') => {
    dispatch(setIncomeToTrainer(text));
  };

  const validateState = (): boolean => {
    const newErrors = validateWorkoutType(state);

    setErrors(newErrors);

    return Object.keys(newErrors).length === 0;
  };

  const handleSubmit = () => {
    const validationResult = validateState();

    if (validationResult) {
      const { name, incomeToCashier, incomeToTrainer } = state;

      onSubmit({
        name,
        id: editData?.id ? editData.id : nanoid(),
        incomeToCashier: Number(incomeToCashier),
        incomeToTrainer: Number(incomeToTrainer),
      } as WorkoutType);
    }
  };

  useEffect(() => {
    if (editData) {
      const { name, incomeToCashier, incomeToTrainer } = editData;

      dispatch(
        setEditData({
          name,
          incomeToCashier: incomeToCashier.toString(),
          incomeToTrainer: incomeToTrainer.toString(),
        }),
      );
    }
  }, [editData]);

  const isEdit = !!editData;

  return (
    <Modal isOpen={isOpen} onClose={onClose} size='xl'>
      <Modal.Content maxH='450'>
        <Modal.CloseButton />
        <Modal.Header>
          {isEdit
            ? t`workoutTypeModal.title.edit`
            : t`workoutTypeModal.title.create`}
        </Modal.Header>
        <Modal.Body>
          <ScrollView>
            <FormControl isRequired mb={2} isInvalid={!!errors.name}>
              <Stack mx='2'>
                <FormControl.Label>{t`workoutTypeModal.fieldsLabels.name`}</FormControl.Label>

                <Input
                  value={state.name}
                  onChangeText={handleChangeName}
                  autoCapitalize='none'
                />

                {errors.name && (
                  <FormControl.ErrorMessage
                    leftIcon={<WarningOutlineIcon size='xs' />}
                  >
                    {errors.name.message}
                  </FormControl.ErrorMessage>
                )}
              </Stack>
            </FormControl>

            <FormControl mb={2} isRequired isInvalid={!!errors.incomeToCashier}>
              <Stack mx='2'>
                <FormControl.Label>{t`workoutTypeModal.fieldsLabels.incomeToOrganization`}</FormControl.Label>

                <Input
                  value={`${state.incomeToCashier}`}
                  onChangeText={handleChangeIncomeToCashier}
                  autoCapitalize='none'
                />

                {errors.incomeToCashier && (
                  <FormControl.ErrorMessage>
                    {errors.incomeToCashier.message}
                  </FormControl.ErrorMessage>
                )}
              </Stack>
            </FormControl>

            <FormControl isRequired mb={2} isInvalid={!!errors.incomeToTrainer}>
              <Stack mx='2'>
                <FormControl.Label>{t`workoutTypeModal.fieldsLabels.incomeToTrainer`}</FormControl.Label>

                <Input
                  value={`${state.incomeToTrainer}`}
                  onChangeText={handleChangeIncomeToTrainer}
                  autoCapitalize='none'
                />

                {errors.incomeToTrainer && (
                  <FormControl.ErrorMessage>
                    {errors.incomeToTrainer.message}
                  </FormControl.ErrorMessage>
                )}
              </Stack>
            </FormControl>
          </ScrollView>
        </Modal.Body>
        <Modal.Footer>
          <Button.Group space={2}>
            <Button variant='ghost' colorScheme='blueGray' onPress={onClose}>
              {t`workoutTypeModal.buttons.cancel`}
            </Button>

            <Button onPress={handleSubmit}>
              {t`workoutTypeModal.buttons.save`}
            </Button>
          </Button.Group>
        </Modal.Footer>
      </Modal.Content>
    </Modal>
  );
};

export default WorkoutTypeModal;
