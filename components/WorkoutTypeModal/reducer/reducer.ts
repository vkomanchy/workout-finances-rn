import { actionsTypes } from './actions';
import { ActionType } from '../../../types/settings';
import { WorkoutTypeState } from '../types';

export const initialState: WorkoutTypeState = {
  name: '',
  incomeToCashier: '',
  incomeToTrainer: '',
};

export const reducer = (
  state: WorkoutTypeState,
  action: ActionType<WorkoutTypeState>,
) => {
  switch (action.type) {
    case actionsTypes.SET_NAME: {
      return {
        ...state,
        ...action.payload,
      };
    }

    case actionsTypes.SET_INCOME_TO_CASHIER: {
      return {
        ...state,
        ...action.payload,
      };
    }

    case actionsTypes.SET_INCOME_TO_TRAINER: {
      return {
        ...state,
        ...action.payload,
      };
    }

    case actionsTypes.SET_EDIT_DATA: {
      return {
        ...state,
        ...action.payload,
      };
    }

    default:
      return state;
  }
};
