import { WorkoutTypeState } from '../types';

export const actionsTypes = {
  SET_NAME: 'SET_NAME',
  SET_INCOME_TO_CASHIER: 'SET_INCOME_TO_CASHIER',
  SET_INCOME_TO_TRAINER: 'SET_INCOME_TO_TRAINER',
  SET_EDIT_DATA: 'SET_EDIT_DATA',
};

export const setName = (name: string) => ({
  type: actionsTypes.SET_NAME,
  payload: {
    name,
  },
});

export const setIncomeToCashier = (incomeToCashier: string) => ({
  type: actionsTypes.SET_INCOME_TO_CASHIER,
  payload: {
    incomeToCashier,
  },
});

export const setIncomeToTrainer = (incomeToTrainer: string) => ({
  type: actionsTypes.SET_INCOME_TO_TRAINER,
  payload: {
    incomeToTrainer,
  },
});

export const setEditData = (data: Partial<WorkoutTypeState>) => ({
  type: actionsTypes.SET_EDIT_DATA,
  payload: data,
});
