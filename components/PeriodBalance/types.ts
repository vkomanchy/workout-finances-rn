import { WorkoutRecordWithExpandedWorkoutType } from '../../calculations/types';

export interface PeriodBalanceProps {
  workoutRecords: WorkoutRecordWithExpandedWorkoutType[];
}
