import { Divider, HStack, Text } from 'native-base';
import { useTranslation } from 'react-i18next';

import { PeriodBalanceProps } from './types';
import {
  calculateTotalForWorkouts,
  getOrgIncome,
  getTrainerIncome,
} from '../../calculations';

const PeriodBalance: React.FC<PeriodBalanceProps> = ({ workoutRecords }) => {
  const { t } = useTranslation();

  const getTotalForWorkouts = calculateTotalForWorkouts(workoutRecords);

  const totalForTrainer = getTotalForWorkouts(getTrainerIncome);
  const totalForOrganization = getTotalForWorkouts(getOrgIncome);

  const total = totalForOrganization + totalForTrainer;

  return (
    <HStack m={4}>
      <Text
        fontSize='lg'
        _light={{
          color: 'green.500',
        }}
        _dark={{
          color: 'green.400',
        }}
        fontWeight='500'
      >
        {t('periodBalance.trainerTotal', { totalForTrainer })}
      </Text>

      <Divider mx={2} thickness='2' orientation='vertical' />

      <Text fontSize='lg' fontWeight='500'>
        {t('periodBalance.total', { total })}
      </Text>
    </HStack>
  );
};

export default PeriodBalance;
