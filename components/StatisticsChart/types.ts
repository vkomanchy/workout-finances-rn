import { CalcPeriod } from '../../types/settings';
import { WorkoutRecordWithExpandedWorkoutType } from '../../calculations/types';

export interface StatisticsChartProps {
  workoutRecords: WorkoutRecordWithExpandedWorkoutType[];
  chartType: 'line' | 'bar' | 'pie' | 'contribution';
  title: string;

  periodTypes: CalcPeriod[];
}
