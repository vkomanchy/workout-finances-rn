import { CalcPeriod } from '../../../types/settings';

import {
  Period,
  WorkoutRecordWithExpandedWorkoutType,
} from '../../../calculations/types';

export interface PeriodWithPeriodType extends Period {
  periodType: CalcPeriod;
}

export interface ChartByTypeProps {
  currentWorkoutRecords: WorkoutRecordWithExpandedWorkoutType[];
  chartType: 'line' | 'bar' | 'pie' | 'contribution';
  currentPeriod: PeriodWithPeriodType;
}

export interface ChartProps
  extends Pick<ChartByTypeProps, 'currentPeriod' | 'currentWorkoutRecords'> {
  screenWidth: number;
}
