import { useMemo } from 'react';

import { ContributionGraph } from 'react-native-chart-kit';
import { DateTime } from 'luxon';
import { Box } from 'native-base';

import { countWorkoutsByDate } from './countWorkoutsByDate';
import { ChartProps } from './types';

const ContributionChart: React.FC<ChartProps> = ({
  currentWorkoutRecords,
  currentPeriod,
  screenWidth,
}) => {
  const workoutsByDate = useMemo(
    () => countWorkoutsByDate(currentWorkoutRecords),
    [currentWorkoutRecords],
  );

  const contributionChartValues = useMemo(
    () =>
      Object.entries(workoutsByDate).map(([key, value]) => ({
        date: key,
        count: value,
      })),
    [workoutsByDate],
  );

  if (!contributionChartValues || contributionChartValues.length <= 0) {
    return null;
  }

  return (
    <Box width='100%' alignItems='center' pb='100px'>
      <ContributionGraph
        tooltipDataAttrs={null}
        values={contributionChartValues}
        endDate={DateTime.fromMillis(currentPeriod?.endDate).toJSDate()}
        numDays={370}
        showOutOfRangeDays
        width={screenWidth}
        height={220}
        chartConfig={{
          backgroundColor: '#e26a00',
          backgroundGradientFrom: '#fb8c00',
          backgroundGradientTo: '#ffa726',
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          style: {
            borderRadius: 16,
          },
        }}
      />
    </Box>
  );
};

export default ContributionChart;
