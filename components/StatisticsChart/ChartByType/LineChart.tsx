import { Box } from 'native-base';
import { useMemo } from 'react';

import { LineChart as LineChartVendor } from 'react-native-chart-kit';
import { ChartProps } from './types';
import { countWorkoutInPeriod } from './countWorkoutInPeriod';
import { getTrainerIncome } from '../../../calculations';

const LineChart: React.FC<ChartProps> = ({
  screenWidth,
  currentWorkoutRecords,
  currentPeriod,
}) => {
  const lineDataset = useMemo(() => {
    if (!currentPeriod) return null;

    return countWorkoutInPeriod(
      currentPeriod.periodType,
      currentWorkoutRecords,
      getTrainerIncome,
    );
  }, [currentPeriod, currentWorkoutRecords]);

  if (!lineDataset) return null;

  return (
    <Box width='100%' alignItems='center'>
      <LineChartVendor
        data={lineDataset}
        width={screenWidth}
        height={220}
        yAxisLabel='₽'
        yAxisSuffix=''
        yAxisInterval={2} // optional, defaults to 1
        chartConfig={{
          backgroundColor: '#e26a00',
          backgroundGradientFrom: '#fb8c00',
          backgroundGradientTo: '#ffa726',
          decimalPlaces: 1, // optional, defaults to 2dp
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          style: {
            borderRadius: 16,
          },
          propsForDots: {
            r: '6',
            strokeWidth: '2',
            stroke: '#ffa726',
          },
        }}
        bezier
        style={{
          marginVertical: 8,
          borderRadius: 16,
        }}
      />
    </Box>
  );
};

export default LineChart;
