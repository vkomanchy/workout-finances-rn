import { Box } from 'native-base';
import { useMemo } from 'react';

import { PieChart as PieChartVendor } from 'react-native-chart-kit';
import { countPeopleByWorkoutType } from '../../../calculations';

import { ChartProps } from './types';

const getRandomColor = () =>
  `#${(0x1000000 + Math.random() * 0xffffff).toString(16).slice(0, 6)}`;

const PieChart: React.FC<ChartProps> = ({
  currentWorkoutRecords,
  screenWidth,
}) => {
  const legendFontSize = 15;
  const legendFontColor = '#7F7F7F';

  const peopleCountByWorkoutType = useMemo(
    () => countPeopleByWorkoutType(currentWorkoutRecords),
    [currentWorkoutRecords],
  );

  const pieData = useMemo(
    () =>
      Object.values(peopleCountByWorkoutType).map((workout) => ({
        ...workout,
        color: getRandomColor(),
        legendFontColor,
        legendFontSize,
      })),
    [peopleCountByWorkoutType],
  );

  return (
    <Box width='100%' alignItems='center' pb='100px'>
      <PieChartVendor
        data={pieData}
        width={screenWidth}
        height={250}
        chartConfig={{
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          style: {
            borderRadius: 16,
          },
          propsForDots: {
            r: '6',
            strokeWidth: '2',
            stroke: '#ffa726',
          },
        }}
        accessor='peopleCount'
        backgroundColor='transparent'
        paddingLeft='0'
        center={[15, 10]}
        absolute
      />
    </Box>
  );
};

export default PieChart;
