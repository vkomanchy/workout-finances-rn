import { DateTime } from 'luxon';
import * as R from 'ramda';
import { CalcPeriod } from '../../../types/settings';
import { WorkoutRecordWithExpandedWorkoutType } from '../../../calculations/types';

// TODO: add unit tests
export const countWorkoutInPeriod = (
  periodType: CalcPeriod,
  currentWorkoutRecords: WorkoutRecordWithExpandedWorkoutType[],
  calcFieldFunc: (workout: WorkoutRecordWithExpandedWorkoutType) => number,
) => {
  const groupedData = R.groupBy((workout) => {
    const date = DateTime.fromMillis(workout.date);

    const dateFormat = periodType === 'year' ? 'LLLL y' : 'dd/LL/yy';

    return date.toFormat(dateFormat);
  }, currentWorkoutRecords);

  if (Object.keys(groupedData).length === 0) return null;

  return Object.entries(groupedData).reduce(
    (acc, current) => {
      const [label, values] = current;

      const fieldData = R.map(calcFieldFunc, values);

      const value = R.sum(fieldData);

      acc.labels.push(label);
      acc.datasets[0].data.push(value);

      return acc;
    },
    {
      labels: [],
      datasets: [
        {
          data: [],
        },
      ],
    },
  );
};
