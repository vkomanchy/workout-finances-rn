import { DateTime } from 'luxon';
import * as R from 'ramda';
import { WorkoutRecordWithExpandedWorkoutType } from '../../../calculations/types';

// TODO: add unit tests
export const countWorkoutsByDate = (
  workouts: WorkoutRecordWithExpandedWorkoutType[],
) =>
  R.reduce(
    (acc, current) => {
      const date = R.path(['date'], current);
      const peopleCount = R.path(['peopleCount'], current);

      const key = DateTime.fromMillis(date).toFormat('yyyy-LL-dd');

      if (acc[key]) {
        acc[key] += peopleCount;
      } else {
        acc[key] = peopleCount;
      }

      return acc;
    },
    {},
    workouts,
  );
