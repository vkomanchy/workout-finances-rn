import { Dimensions } from 'react-native';

import { ChartByTypeProps } from './types';
import ContributionChart from './ContributionChart';
import PieChart from './PieChart';
import BarChart from './BarChart';
import LineChart from './LineChart';

const ChartByType: React.FC<ChartByTypeProps> = ({
  chartType,
  currentWorkoutRecords,

  currentPeriod,
}) => {
  const screenWidth = Dimensions.get('window').width - 50;

  if (!currentPeriod) return null;

  if (chartType === 'contribution') {
    return (
      <ContributionChart
        currentWorkoutRecords={currentWorkoutRecords}
        currentPeriod={currentPeriod}
        screenWidth={screenWidth}
      />
    );
  }

  if (chartType === 'pie') {
    return (
      <PieChart
        currentWorkoutRecords={currentWorkoutRecords}
        currentPeriod={currentPeriod}
        screenWidth={screenWidth}
      />
    );
  }

  if (chartType === 'bar') {
    return (
      <BarChart
        currentWorkoutRecords={currentWorkoutRecords}
        currentPeriod={currentPeriod}
        screenWidth={screenWidth}
      />
    );
  }

  return (
    <LineChart
      currentWorkoutRecords={currentWorkoutRecords}
      currentPeriod={currentPeriod}
      screenWidth={screenWidth}
    />
  );
};

export default ChartByType;
