import { useTranslation } from 'react-i18next';
import { Box, CheckIcon, Heading, HStack, Select, VStack } from 'native-base';
import { useEffect, useMemo, useState } from 'react';

import * as R from 'ramda';

import { StatisticsChartProps } from './types';
import ChartByType from './ChartByType';
import { createPeriodsForWorkoutsList } from '../../calculations/getPeriods';
import { CalcPeriod } from '../../types/settings';
import { getWorkoutsForPeriod } from '../../calculations/getWorkoutsForPeriod';
import PeriodPicker from '../PeriodPicker';
import { ChangePeriodValues } from '../PeriodPicker/types';

const StatisticsChart: React.FC<StatisticsChartProps> = ({
  workoutRecords,
  chartType,
  title,
  periodTypes,
}) => {
  const { t } = useTranslation();
  const [selectedPeriodType, setSelectedPeriodType] = useState(periodTypes[0]);

  const [currentPeriod, setCurrentPeriod] = useState({
    startDate: 1689875639781,
    endDate: 1689875639781,
    periodType: periodTypes[0],
  });

  const periods = useMemo(
    () =>
      R.map(
        (period) => ({
          ...period,
          periodType: selectedPeriodType,
        }),
        createPeriodsForWorkoutsList(workoutRecords, selectedPeriodType),
      ),
    [selectedPeriodType, workoutRecords],
  );

  const workoutsForCurrentPeriod = useMemo(() => {
    if (currentPeriod) {
      return getWorkoutsForPeriod(workoutRecords, currentPeriod);
    }

    return [];
  }, [currentPeriod, workoutRecords]);

  const handleChangeCurrentPeriod = (value: ChangePeriodValues) => {
    const currentPeriodIndex = R.indexOf(currentPeriod, periods);

    if (value === 'minus') {
      if (currentPeriodIndex <= 0) {
        throw new Error(
          'currentPeriod could not be changed because of negative index',
        );
      }

      setCurrentPeriod(periods[currentPeriodIndex - 1]);
    } else {
      if (currentPeriodIndex < 0 || currentPeriodIndex + 1 === periods.length) {
        throw new Error(
          'currentPeriod could not be changed because of exceeding periods',
        );
      }

      setCurrentPeriod(periods[currentPeriodIndex + 1]);
    }
  };

  const periodsAvailability = useMemo(() => {
    const indexOfCurrentPeriod = R.indexOf(currentPeriod, periods);

    if (indexOfCurrentPeriod < 0) {
      return {
        isMinusAvailable: false,
        isPlusAvailable: false,
      };
    }

    return {
      isMinusAvailable: indexOfCurrentPeriod > 0,
      isPlusAvailable:
        indexOfCurrentPeriod >= 0 && indexOfCurrentPeriod < periods.length - 1,
    };
  }, [currentPeriod, periods]);

  useEffect(() => {
    if (periods.length > 0) {
      setCurrentPeriod(periods[0]);
    }
  }, [periods, selectedPeriodType]);

  const isPeriodChangeable = periodTypes.length > 1;

  return (
    <VStack>
      <Heading size='md' m={4}>
        {title}
      </Heading>

      {isPeriodChangeable && (
        <HStack m={4} justifyContent='space-between'>
          <Select
            selectedValue={selectedPeriodType}
            minWidth='100'
            maxW='100'
            accessibilityLabel={t`statistics.charts.periodLabel`}
            placeholder={t`statistics.charts.periodLabel`}
            _selectedItem={{
              bg: 'teal.600',
              endIcon: <CheckIcon size='5' />,
            }}
            mt={1}
            onValueChange={(value) =>
              setSelectedPeriodType(value as CalcPeriod)
            }
          >
            {periodTypes.map((value) => (
              <Select.Item
                label={t(`statistics.periodsLabels.${value}`)}
                key={value}
                value={value}
              />
            ))}
          </Select>

          <PeriodPicker
            currentPeriod={currentPeriod}
            onChangePeriod={handleChangeCurrentPeriod}
            periodType={selectedPeriodType}
            periodsAvailability={periodsAvailability}
            style='inline'
          />
        </HStack>
      )}

      <Box width='100%' alignItems='center'>
        <ChartByType
          chartType={chartType}
          currentWorkoutRecords={workoutsForCurrentPeriod}
          currentPeriod={currentPeriod}
        />
      </Box>
    </VStack>
  );
};

export default StatisticsChart;
