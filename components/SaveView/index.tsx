import { useSafeAreaInsets } from 'react-native-safe-area-context';

import { View } from 'react-native';
import { useColorMode, useTheme } from 'native-base';

export default function SafeView({ children }) {
  const { colorMode } = useColorMode();
  const insets = useSafeAreaInsets();
  const { colors } = useTheme();

  return (
    <View
      style={{
        flex: 1,

        // Paddings to handle safe area
        paddingTop: insets.top,
        paddingBottom: insets.bottom,
        paddingLeft: insets.left,
        paddingRight: insets.right,

        backgroundColor:
          colorMode === 'light' ? colors.light[200] : colors.blueGray[700],
      }}
    >
      {children}
    </View>
  );
}
