import {
  Pressable,
  Button,
  HStack,
  Icon,
  IconButton,
  Popover,
  Text,
  Badge,
} from 'native-base';

import { useTranslation } from 'react-i18next';
import { useState } from 'react';

import { DateTime } from 'luxon';

import AntDesignIcon from 'react-native-vector-icons/AntDesign';

import { WorkoutRecordProps } from './types';
import { getOrgIncome, getTrainerIncome } from '../../../calculations';

const WorkoutRecord: React.FC<WorkoutRecordProps> = ({
  workoutRecord,
  onEdit,
  onDelete,
}) => {
  const { t } = useTranslation();

  const [isExpanded, setIsExpanded] = useState(false);

  const { id, peopleCount, date, workoutType, markColor } = workoutRecord;

  const formattedDate = DateTime.fromMillis(date).toLocaleString(
    isExpanded ? DateTime.DATETIME_MED : DateTime.DATE_SHORT,
  );

  const countedIncomeToTrainer = getTrainerIncome(workoutRecord);
  const countedIncomeToOrganization = getOrgIncome(workoutRecord);

  return (
    <Pressable
      onPress={() => {
        setIsExpanded((prev) => !prev);
      }}
      flexBasis={isExpanded ? '96%' : '30%'}
      marginRight='3%'
      key={id}
      padding='4'
      minH='150px'
      mb='4'
      maxWidth={isExpanded ? null : '250px'}
      rounded='lg'
      overflow='hidden'
      borderColor='coolGray.200'
      borderWidth='1'
      _dark={{
        borderColor: 'coolGray.600',
        backgroundColor: markColor || 'gray.700',
      }}
      _web={{
        shadow: 2,
        borderWidth: 0,
      }}
      _light={{
        backgroundColor: markColor || 'gray.50',
      }}
    >
      <HStack alignItems='center'>
        <Text fontWeight='bold' fontSize={isExpanded ? 'md' : 'xs'} my={2}>
          {formattedDate}
        </Text>

        {isExpanded && (
          <HStack justifyContent='flex-end' flexGrow='1'>
            <IconButton
              icon={<Icon as={AntDesignIcon} name='edit' />}
              borderRadius='full'
              _icon={{
                size: 'xs',
              }}
              onPress={() => {
                onEdit(workoutRecord);
              }}
            />

            <Popover
              trigger={(triggerProps) => (
                <IconButton
                  id='delete-button'
                  icon={<Icon as={AntDesignIcon} name='delete' />}
                  borderRadius='full'
                  _icon={{
                    color: 'red.500',
                    size: 'xs',
                  }}
                  {...triggerProps}
                />
              )}
            >
              <Popover.Content accessibilityLabel='Delete workout' w='56'>
                <Popover.CloseButton />

                <Popover.Header>
                  {t`workoutRecord.deleteWorkoutPopover.title`}
                </Popover.Header>

                <Popover.Body>
                  {t('workoutRecord.deleteWorkoutPopover.body', {
                    formattedDate,
                  })}
                </Popover.Body>

                <Popover.Footer justifyContent='flex-end'>
                  <Button.Group space={2}>
                    <Button
                      colorScheme='danger'
                      onPress={() => {
                        onDelete(id);
                      }}
                    >
                      {t`workoutRecord.deleteWorkoutPopover.deleteButton`}
                    </Button>
                  </Button.Group>
                </Popover.Footer>
              </Popover.Content>
            </Popover>
          </HStack>
        )}
      </HStack>

      {isExpanded && (
        <>
          <Badge colorScheme='info' variant='outline' maxW='100px'>
            {workoutType?.name}
          </Badge>

          <Text>{t('workoutRecord.peopleCount', { peopleCount })}</Text>
        </>
      )}

      <Text
        _light={{
          color: 'green.500',
        }}
        _dark={{
          color: 'green.400',
        }}
        fontSize={isExpanded ? 'md' : 'xs'}
      >
        {isExpanded
          ? t('workoutRecord.incomeToTrainer', {
              incomeToTrainer: countedIncomeToTrainer,
            })
          : `T - ${countedIncomeToTrainer}`}
        &#x20bd;
      </Text>

      <Text
        _light={{
          color: 'violet.500',
        }}
        _dark={{
          color: 'violet.400',
        }}
        fontSize={isExpanded ? 'md' : 'xs'}
      >
        {isExpanded
          ? t('workoutRecord.incomeToOrganization', {
              incomeToOrganization: countedIncomeToOrganization,
            })
          : `O - ${countedIncomeToOrganization}`}
        &#x20bd;
      </Text>
    </Pressable>
  );
};

export default WorkoutRecord;
