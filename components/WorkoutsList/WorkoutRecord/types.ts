import {
  MarkedWorkoutRecord,
  WorkoutRecordWithExpandedWorkoutType,
} from '../../../calculations/types';

export interface WorkoutRecordProps {
  workoutRecord: MarkedWorkoutRecord;
  onEdit: (workoutRecord: WorkoutRecordWithExpandedWorkoutType) => void;
  onDelete: (workoutId: string) => void;
}
