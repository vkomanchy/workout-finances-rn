import { WorkoutRecordWithExpandedWorkoutType } from '../../calculations/types';

export interface WorkoutsListProps {
  workoutRecords: WorkoutRecordWithExpandedWorkoutType[];
  onEdit: (workoutRecord: WorkoutRecordWithExpandedWorkoutType) => void;
  onDelete: (id: string) => void;
}
