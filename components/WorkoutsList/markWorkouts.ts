import { DateTime } from 'luxon';

import * as R from 'ramda';

import {
  MarkedWorkoutRecord,
  WorkoutRecordWithExpandedWorkoutType,
} from '../../calculations/types';
import { Theme } from '../../store/types';
import { getRandomColor } from '../getRandomColor';

// TODO: add unit tests
export const markWorkoutsWithSameDate = (
  workoutRecords: WorkoutRecordWithExpandedWorkoutType[],
  theme: Theme,
): MarkedWorkoutRecord[] => {
  const groupedWorkouts = R.groupBy(
    (workoutRecord: WorkoutRecordWithExpandedWorkoutType) => {
      const date = DateTime.fromMillis(workoutRecord.date);

      const keyForGroup = `${date.year}-${date.month}-${date.day}-${date.hour}-${date.minute}`;

      return keyForGroup;
    },
    workoutRecords,
  );

  return R.reduce(
    (acc, workouts) => {
      if (workouts.length <= 1) {
        acc.push(...workouts);

        return acc;
      }

      const markColor = getRandomColor(theme);

      const workoutsWithMark = R.map(
        (workout) => ({ ...workout, markColor }),
        workouts,
      );

      acc.push(...workoutsWithMark);

      return acc;
    },
    [],
    R.values(groupedWorkouts),
  );
};
