import { Box, ScrollView } from 'native-base';
import { useMemo, useContext } from 'react';

import { WorkoutsListProps } from './types';
import type { WorkoutRecordWithExpandedWorkoutType } from '../../calculations/types';
import WorkoutRecord from './WorkoutRecord';
import { markWorkoutsWithSameDate } from './markWorkouts';
import { sortWorkouts } from '../../calculations/sortWorkouts';

import { AppContext } from '../../store';

const WorkoutsList: React.FC<WorkoutsListProps> = ({
  workoutRecords,
  onEdit,
  onDelete,
}) => {
  const { theme } = useContext(AppContext);

  const handleEditClick = (
    workoutRecord: WorkoutRecordWithExpandedWorkoutType,
  ) => {
    onEdit(workoutRecord);
  };

  const handleDelete = (id: string) => {
    onDelete(id);
  };

  const markedWorkouts = useMemo(
    () => sortWorkouts(markWorkoutsWithSameDate(workoutRecords, theme)),
    [workoutRecords, theme],
  );

  return (
    <ScrollView>
      <Box
        display='flex'
        flexWrap='wrap'
        alignItems='center'
        justifyContent='space-between'
        w='100%'
        flexDirection='row'
        p={4}
      >
        {markedWorkouts.map((workoutRecord) => (
          <WorkoutRecord
            key={workoutRecord.id}
            workoutRecord={workoutRecord}
            onDelete={handleDelete}
            onEdit={handleEditClick}
          />
        ))}
      </Box>
    </ScrollView>
  );
};

export default WorkoutsList;
