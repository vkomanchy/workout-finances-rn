import { WorkoutRecordWithExpandedWorkoutType } from '../../calculations/types';

export interface PeriodPeopleCountProps {
  workoutRecords: WorkoutRecordWithExpandedWorkoutType[];
}
