import { Button, HStack, Popover, Text } from 'native-base';
import { useMemo } from 'react';

import * as R from 'ramda';

import { useTranslation } from 'react-i18next';
import {
  calculateTotalForWorkouts,
  countPeopleByWorkoutType,
  getPeopleCount,
} from '../../calculations';

import { PeriodPeopleCountProps } from './types';

const PeriodPeopleCount: React.FC<PeriodPeopleCountProps> = ({
  workoutRecords,
}) => {
  const { t } = useTranslation();

  const peopleCountByWorkoutType = useMemo(
    () => countPeopleByWorkoutType(workoutRecords),
    [workoutRecords],
  );

  const totalPeopleCount = useMemo(
    () => calculateTotalForWorkouts(workoutRecords, getPeopleCount),
    [workoutRecords],
  );

  return (
    <HStack my={2} mx={4}>
      <Text>{t('periodPeopleCount.title')}</Text>

      <Popover
        trigger={(triggerProps) => (
          <Button {...triggerProps} variant='link' size='md' p='0'>
            {totalPeopleCount}
          </Button>
        )}
      >
        <Popover.Content accessibilityLabel='Delete Customerd' w='56'>
          <Popover.Arrow />
          <Popover.CloseButton />
          <Popover.Header>{t`periodPeopleCount.popover.title`}</Popover.Header>

          <Popover.Body>
            {R.values(peopleCountByWorkoutType).map(
              ({ peopleCount, name, id }) => (
                <HStack key={id}>
                  <Text>
                    {name} - {peopleCount}
                  </Text>
                </HStack>
              ),
            )}
          </Popover.Body>
        </Popover.Content>
      </Popover>
    </HStack>
  );
};

export default PeriodPeopleCount;
