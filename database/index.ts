import AsyncStorage from '@react-native-async-storage/async-storage';

export const storeObjectData = async (key: string, value) => {
  try {
    const jsonValue = JSON.stringify(value);

    await AsyncStorage.setItem(key, jsonValue);
  } catch (e) {
    // saving error
  }
};

export const getObjectData = async (key: string) => {
  try {
    const value = await AsyncStorage.getItem(key);

    if (value == null) {
      throw new Error('Error while reading data');
    }

    return JSON.parse(value);
  } catch (e) {
    // error reading value

    return null;
  }
};
