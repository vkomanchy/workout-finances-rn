import { WorkoutType } from '../calculations/types';

export const addWorkoutType = (
  data: WorkoutType,
  workoutsTypesList: WorkoutType[],
) => [...workoutsTypesList, data];
