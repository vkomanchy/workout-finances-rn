import { WORKOUT_TYPES_STORAGE_KEY } from '../constants';
import { getObjectData } from '../database';

export const fetchWorkoutTypes = async () => {
  try {
    const workoutTypes = await getObjectData(WORKOUT_TYPES_STORAGE_KEY);

    if (!workoutTypes) throw new Error('Not found workout types');

    return workoutTypes;
  } catch (error) {
    console.error('Error while fetching workout types', error);

    return [];
  }
};
