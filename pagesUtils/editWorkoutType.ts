import * as R from 'ramda';
import { WorkoutType } from '../calculations/types';

export const editWorkoutType = (
  updatedData: WorkoutType,
  workoutsTypesList: WorkoutType[],
) => {
  const indexOfItemForUpdate = R.findIndex(
    R.propEq(updatedData.id, 'id'),
    workoutsTypesList,
  );

  if (indexOfItemForUpdate < 0) {
    throw new Error('Updated workout type not found in workouts types list');
  }

  return R.update(indexOfItemForUpdate, updatedData, workoutsTypesList);
};
