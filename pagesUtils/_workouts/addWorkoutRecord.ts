import {
  WorkoutRecord,
  WorkoutRecordWithWorkoutTypeId,
} from '../../calculations/types';

export const addWorkoutRecord = (
  data: WorkoutRecordWithWorkoutTypeId,
  workoutRecords: WorkoutRecord[],
) => [...workoutRecords, data];
