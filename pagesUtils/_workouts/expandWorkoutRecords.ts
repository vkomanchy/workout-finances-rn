import * as R from 'ramda';
import {
  WorkoutRecordWithExpandedWorkoutType,
  WorkoutRecordWithWorkoutTypeId,
  WorkoutType,
} from '../../calculations/types';

export const expandWorkoutRecords = (
  workoutRecords: WorkoutRecordWithWorkoutTypeId[],
  workoutTypes: WorkoutType[],
): WorkoutRecordWithExpandedWorkoutType[] => {
  const workoutTypeByIds: { [key: string]: WorkoutType } = R.reduce(
    (acc, workoutType) => {
      const workoutTypeId = R.prop('id', workoutType);

      acc[workoutTypeId] = workoutType;

      return acc;
    },
    {},
    workoutTypes,
  );

  return R.map(
    (workoutRecord) => ({
      ...workoutRecord,
      workoutType: workoutTypeByIds[R.prop('workoutType', workoutRecord)],
    }),
    workoutRecords,
  );
};
