import * as R from 'ramda';

import { WorkoutRecord } from '../../calculations/types';

export const filterWorkoutRecords = (
  idForRemoving: string,
  workoutTypesList: WorkoutRecord[],
) => R.filter(({ id }) => id !== idForRemoving, workoutTypesList);
