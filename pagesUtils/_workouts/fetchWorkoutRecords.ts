import { WORKOUT_RECORD_STORAGE_KEY } from '../../constants';
import { getObjectData } from '../../database';

export const fetchWorkoutRecords = async () => {
  try {
    const workoutRecords = await getObjectData(WORKOUT_RECORD_STORAGE_KEY);

    if (!workoutRecords) throw new Error('Not found workout records');

    return workoutRecords;
  } catch (error) {
    console.error('Error while fetching workout records', error);

    return [];
  }
};
