import * as R from 'ramda';
import { WorkoutRecord } from '../../calculations/types';

export const editWorkoutRecord = (
  updatedData: WorkoutRecord,
  workoutRecords: WorkoutRecord[],
) => {
  const indexOfItemForUpdate = R.findIndex(
    R.propEq(updatedData.id, 'id'),
    workoutRecords,
  );

  if (indexOfItemForUpdate < 0) {
    throw new Error('Updated workout record not found in workout records');
  }

  return R.update(indexOfItemForUpdate, updatedData, workoutRecords);
};
