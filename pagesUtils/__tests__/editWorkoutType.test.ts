import { expect, it, describe } from 'vitest';
import { editWorkoutType } from '../editWorkoutType';

import { WorkoutType } from '../../calculations/types';

describe('Check addWorkoutType function', () => {
  it('Check that item edited and other objects remains unchanged', () => {
    const workoutTypesList: WorkoutType[] = [
      {
        name: '123456',
        id: '123456',

        incomeToCashier: 50,
        incomeToTrainer: 20,
      },
      {
        name: 'initial name',
        id: 'itemForEdit',

        incomeToCashier: 10,
        incomeToTrainer: 20,
      },
    ];

    const itemForEdit = {
      name: 'result name',
      id: 'itemForEdit',

      incomeToCashier: 50,
      incomeToTrainer: 50,
    };

    const result = editWorkoutType(
      itemForEdit as WorkoutType,
      workoutTypesList as WorkoutType[],
    );

    expect(result).toHaveLength(2);

    expect(result[1]).toMatchObject(itemForEdit);

    expect(result[0]).toMatchObject(workoutTypesList[0]);
  });

  it('Should throw an error if workoutType with given id not found in list', () => {
    const workoutTypesList: WorkoutType[] = [
      {
        name: '123456',
        id: '123456',

        incomeToCashier: 50,
        incomeToTrainer: 20,
      },
    ];

    const itemForEdit = {
      name: 'result name',
      id: 'unknown id',

      incomeToCashier: 50,
      incomeToTrainer: 50,
    };

    expect(() =>
      editWorkoutType(
        itemForEdit as WorkoutType,
        workoutTypesList as WorkoutType[],
      ),
    ).toThrowError(/workout type not found/);
  });
});
