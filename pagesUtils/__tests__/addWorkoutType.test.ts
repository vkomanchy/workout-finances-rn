import { expect, it, describe } from 'vitest';
import { addWorkoutType } from '../addWorkoutType';
import { WorkoutType } from '../../calculations/types';

describe('Check addWorkoutType function', () => {
  it('Check that item was added in the end of the list', () => {
    const workoutTypesList = [
      {
        id: '123456',
      },
      {
        id: 'ajkdwjecv',
      },
    ];

    const itemForAppend = {
      id: 'appended',
    };

    const result = addWorkoutType(
      itemForAppend as WorkoutType,
      workoutTypesList as WorkoutType[],
    );

    expect(result).toHaveLength(3);
    expect(result[2].id).toBe(itemForAppend.id);
  });
});
