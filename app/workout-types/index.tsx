import { useCallback, useEffect, useState, useRef } from 'react';
import { Center, Container, Heading, Fab, Icon } from 'native-base';

import { useTranslation } from 'react-i18next';

import AntDesignIcon from 'react-native-vector-icons/AntDesign';

import { WORKOUT_TYPES_STORAGE_KEY } from '../../constants';
import SafeView from '../../components/SaveView';
import WorkoutsTypesList from '../../components/WorkoutsTypesList';
import WorkoutTypeModal from '../../components/WorkoutTypeModal';
import { WorkoutType } from '../../calculations/types';
import { storeObjectData } from '../../database';
import { addWorkoutType } from '../../pagesUtils/addWorkoutType';
import { editWorkoutType } from '../../pagesUtils/editWorkoutType';
import { fetchWorkoutTypes } from '../../pagesUtils/fetchWorkoutTypes';

export default function WorkoutTypes() {
  const { t } = useTranslation();

  const isMounted = useRef(false);

  const [isModalVisible, setIsModalVisible] = useState(false);

  const [workoutsTypes, setWorkoutTypes] = useState([]);

  const [editData, setEditData] = useState<WorkoutType | null>(null);

  const isEdit = editData && editData.id;

  const handleCloseModal = () => {
    setIsModalVisible(false);
  };

  const handleOpenModal = () => {
    setIsModalVisible(true);
  };

  const handleFetchWorkoutTypes = useCallback(fetchWorkoutTypes, []);

  const handleRefresh = useCallback(async () => {
    const newWorkoutTypes = await handleFetchWorkoutTypes();

    if (isMounted.current) {
      setWorkoutTypes(newWorkoutTypes);
    }
  }, [handleFetchWorkoutTypes]);

  const handleSubmit = async (data: WorkoutType) => {
    setIsModalVisible(false);
    setEditData(null);

    try {
      const currentWorkoutTypes = await handleFetchWorkoutTypes();

      const newWorkoutsTypes: WorkoutType[] = isEdit
        ? editWorkoutType(data, currentWorkoutTypes)
        : addWorkoutType(data, currentWorkoutTypes);

      await storeObjectData(WORKOUT_TYPES_STORAGE_KEY, newWorkoutsTypes);

      setWorkoutTypes(newWorkoutsTypes);
    } catch (error) {
      console.error('Error while updating/creating workout type', error);
    }
  };

  const handleEdit = (data: WorkoutType) => {
    setEditData(data);

    setIsModalVisible(true);
  };

  useEffect(() => {
    isMounted.current = true;

    handleRefresh();

    return () => {
      isMounted.current = false;
    };
  }, [handleRefresh]);

  return (
    <SafeView>
      <Center>
        <Container>
          <Center>
            <Heading bold my={4}>
              {t`workoutTypesTitle`}
            </Heading>

            <WorkoutsTypesList
              workoutsTypes={workoutsTypes}
              onRefresh={handleRefresh}
              onEdit={handleEdit}
            />
          </Center>
        </Container>
      </Center>

      <Fab
        renderInPortal={false}
        shadow={2}
        size='sm'
        onPress={handleOpenModal}
        icon={<Icon color='white' as={AntDesignIcon} name='plus' size='sm' />}
      />

      <WorkoutTypeModal
        key={Date.now()}
        editData={editData}
        isOpen={isModalVisible}
        onClose={handleCloseModal}
        onSubmit={handleSubmit}
      />
    </SafeView>
  );
}
