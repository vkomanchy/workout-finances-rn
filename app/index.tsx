import { Box, Fab, Icon } from 'native-base';

import AntDesignIcon from 'react-native-vector-icons/AntDesign';

import { useContext, useState, useMemo, useRef, useEffect } from 'react';

import * as R from 'ramda';

import { AppContext } from '../store';

import SafeView from '../components/SaveView';
import PeriodPicker from '../components/PeriodPicker';
import { ChangePeriodValues } from '../components/PeriodPicker/types';
import {
  createPeriodsForWorkoutsList,
  getCurrentPeriod,
} from '../calculations/getPeriods';
import {
  Period,
  WorkoutRecordWithExpandedWorkoutType,
  WorkoutRecordWithWorkoutTypeId,
} from '../calculations/types';
import { sortWorkouts } from '../calculations/sortWorkouts';
import WorkoutModal from '../components/WorkoutModal';
import { fetchWorkoutTypes } from '../pagesUtils/fetchWorkoutTypes';
import { addWorkoutRecord } from '../pagesUtils/_workouts/addWorkoutRecord';
import { WORKOUT_RECORD_STORAGE_KEY } from '../constants';
import { storeObjectData } from '../database';
import { fetchWorkoutRecords } from '../pagesUtils/_workouts/fetchWorkoutRecords';
import { expandWorkoutRecords } from '../pagesUtils/_workouts/expandWorkoutRecords';
import { editWorkoutRecord } from '../pagesUtils/_workouts/editWorkoutRecord';
import { filterWorkoutRecords } from '../pagesUtils/_workouts/filterWorkoutRecords';
import WorkoutsList from '../components/WorkoutsList';
import PeriodBalance from '../components/PeriodBalance';
import { getWorkoutsForPeriod } from '../calculations/getWorkoutsForPeriod';
import PeriodPeopleCount from '../components/PeriodPeopleCount';

export default function Workouts() {
  const { calcPeriod } = useContext(AppContext);

  const isMounted = useRef(false);

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [editData, setEditData] = useState(null);

  const [workoutTypes, setWorkoutTypes] = useState([]);

  const [workoutRecords, setWorkoutRecords] = useState([]);

  const isEdit = editData && editData.id;

  const sortedWorkouts = useMemo(
    () => sortWorkouts(workoutRecords),
    [workoutRecords],
  );

  const [currentPeriod, setCurrentPeriod] = useState({
    startDate: 1689875639781,
    endDate: 1689875639781,
  });

  const periods = useMemo(
    () => createPeriodsForWorkoutsList(sortedWorkouts, calcPeriod),
    [calcPeriod, sortedWorkouts],
  );

  const workoutsForCurrentPeriod = useMemo(
    () => getWorkoutsForPeriod(sortedWorkouts, currentPeriod),
    [currentPeriod, sortedWorkouts],
  );

  const periodsAvailability = useMemo(() => {
    const indexOfCurrentPeriod = R.indexOf(currentPeriod, periods);

    if (indexOfCurrentPeriod < 0) {
      return {
        isMinusAvailable: false,
        isPlusAvailable: false,
      };
    }

    return {
      isMinusAvailable: indexOfCurrentPeriod > 0,
      isPlusAvailable:
        indexOfCurrentPeriod >= 0 && indexOfCurrentPeriod < periods.length - 1,
    };
  }, [periods, currentPeriod]);

  const handleChangeCurrentPeriod = (value: ChangePeriodValues) => {
    const currentPeriodIndex = R.indexOf(currentPeriod, periods);

    if (value === 'minus') {
      if (currentPeriodIndex <= 0) {
        throw new Error(
          'currentPeriod could not be changed because of negative index',
        );
      }

      setCurrentPeriod(periods[currentPeriodIndex - 1]);
    } else {
      if (currentPeriodIndex < 0 || currentPeriodIndex + 1 === periods.length) {
        throw new Error(
          'currentPeriod could not be changed because of exceeding periods',
        );
      }

      setCurrentPeriod(periods[currentPeriodIndex + 1]);
    }
  };

  const handleOpenModal = () => {
    setIsModalVisible(true);
  };

  const handleCloseModal = () => {
    setIsModalVisible(false);
  };

  const handleSubmitWorkoutRecord = async (
    workoutRecord: WorkoutRecordWithWorkoutTypeId,
  ) => {
    setIsModalVisible(false);
    setEditData(null);

    try {
      const currentWorkoutRecords = await fetchWorkoutRecords();

      const newWorkoutRecords = isEdit
        ? editWorkoutRecord(workoutRecord, currentWorkoutRecords)
        : addWorkoutRecord(workoutRecord, currentWorkoutRecords);

      await storeObjectData(WORKOUT_RECORD_STORAGE_KEY, newWorkoutRecords);

      setWorkoutRecords(
        expandWorkoutRecords(
          newWorkoutRecords as WorkoutRecordWithWorkoutTypeId[],
          workoutTypes,
        ),
      );
    } catch (error) {
      console.error('Error while updating/creating workout type', error);
    }
  };

  const handleRefresh = async () => {
    const newWorkoutTypes = await fetchWorkoutTypes();
    const newWorkoutRecords = await fetchWorkoutRecords();

    if (isMounted.current) {
      setWorkoutTypes(newWorkoutTypes);
      setWorkoutRecords(
        expandWorkoutRecords(newWorkoutRecords, newWorkoutTypes),
      );
    }
  };

  const handleEdit = (data: WorkoutRecordWithExpandedWorkoutType) => {
    setEditData(data);

    setIsModalVisible(true);
  };

  const handleDeleteWorkoutRecord = async (idForRemoving: string) => {
    const currentWorkoutRecords = await fetchWorkoutRecords();

    const newWorkoutRecords = filterWorkoutRecords(
      idForRemoving,
      currentWorkoutRecords,
    );

    await storeObjectData(WORKOUT_RECORD_STORAGE_KEY, newWorkoutRecords);

    handleRefresh();
  };

  useEffect(() => {
    isMounted.current = true;

    handleRefresh();

    return () => {
      isMounted.current = false;
    };
  }, []);

  useEffect(() => {
    const newCurrentPeriod = getCurrentPeriod(calcPeriod);

    const foundPeriod = R.find<Period>(
      ({ startDate, endDate }) =>
        startDate === newCurrentPeriod.startDate &&
        endDate === newCurrentPeriod.endDate,
      periods,
    );

    if (foundPeriod) {
      setCurrentPeriod(foundPeriod);
    } else if (!periods || periods.length === 0) {
      setCurrentPeriod(newCurrentPeriod);
    } else {
      setCurrentPeriod(periods.at(-1));
    }
  }, [calcPeriod, periods]);

  return (
    <SafeView>
      <Box m={4}>
        <PeriodPicker
          style='heading'
          currentPeriod={currentPeriod}
          periodsAvailability={periodsAvailability}
          periodType={calcPeriod}
          onChangePeriod={handleChangeCurrentPeriod}
        />
      </Box>

      <PeriodBalance workoutRecords={workoutsForCurrentPeriod} />

      <PeriodPeopleCount workoutRecords={workoutsForCurrentPeriod} />

      <WorkoutsList
        workoutRecords={workoutsForCurrentPeriod}
        onEdit={handleEdit}
        onDelete={handleDeleteWorkoutRecord}
      />

      <Fab
        renderInPortal={false}
        shadow={2}
        size='sm'
        onPress={handleOpenModal}
        icon={<Icon color='white' as={AntDesignIcon} name='plus' size='sm' />}
      />

      <WorkoutModal
        key={Date.now()}
        workoutTypes={workoutTypes}
        isOpen={isModalVisible}
        editData={editData}
        onClose={handleCloseModal}
        onSubmit={handleSubmitWorkoutRecord}
      />
    </SafeView>
  );
}
