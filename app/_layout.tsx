import { Tabs } from 'expo-router';
import { NativeBaseProvider, useColorMode, useTheme } from 'native-base';
import { useReducer, useCallback, useEffect, useContext } from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import { I18nextProvider, useTranslation } from 'react-i18next';

import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import { Appearance } from 'react-native';
import { reducer as appReducer, initialState } from '../store/_reducer/reducer';
import { AppContext, AppDispatchContext } from '../store';
import { getObjectData } from '../database';
import { setSettingsData } from '../store/_reducer/actions';
import i18n from '../i18n';

const TabsWithLocalization = () => {
  const { t } = useTranslation();
  const { theme } = useContext(AppContext);

  const { colors } = useTheme();
  const { colorMode, toggleColorMode } = useColorMode();

  useEffect(() => {
    let currentTheme = theme;

    if (theme === 'os') {
      currentTheme = Appearance.getColorScheme();
    }

    if (colorMode !== currentTheme) {
      toggleColorMode();
    }
  }, [colorMode, theme, toggleColorMode]);

  return (
    <Tabs>
      <Tabs.Screen
        name='index'
        options={{
          header: () => null,
          tabBarLabel: t`tabs.list`,
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcon
              name='format-list-bulleted'
              color={color}
              size={size}
            />
          ),
          tabBarStyle: {
            backgroundColor:
              colorMode === 'light' ? colors.light[200] : colors.blueGray[700],
            borderTopColor:
              colorMode === 'light' ? colors.light[100] : colors.blueGray[800],
          },
          title: t`pageTitles.list`,
          unmountOnBlur: true,
        }}
      />

      <Tabs.Screen
        name='workout-types/index'
        options={{
          header: () => null,
          tabBarLabel: t`tabs.workoutTypes`,
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcon
              name='playlist-plus'
              color={color}
              size={size}
            />
          ),
          title: t`pageTitles.workoutTypes`,
          unmountOnBlur: true,
          tabBarStyle: {
            backgroundColor:
              colorMode === 'light' ? colors.light[200] : colors.blueGray[700],
            borderTopColor:
              colorMode === 'light' ? colors.light[100] : colors.blueGray[800],
          },
        }}
      />

      <Tabs.Screen
        name='statistics/index'
        options={{
          header: () => null,
          tabBarLabel: t`tabs.statistics`,
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcon
              name='chart-bar-stacked'
              color={color}
              size={size}
            />
          ),
          tabBarStyle: {
            backgroundColor:
              colorMode === 'light' ? colors.light[200] : colors.blueGray[700],
            borderTopColor:
              colorMode === 'light' ? colors.light[100] : colors.blueGray[800],
          },
          title: t`pageTitles.statistics`,
          unmountOnBlur: true,
        }}
      />

      <Tabs.Screen
        name='settings/index'
        options={{
          header: () => null,
          tabBarLabel: t`tabs.settings`,
          tabBarIcon: ({ color, size }) => (
            <MaterialIcon name='settings' color={color} size={size} />
          ),
          tabBarStyle: {
            backgroundColor:
              colorMode === 'light' ? colors.light[200] : colors.blueGray[700],
            borderTopColor:
              colorMode === 'light' ? colors.light[100] : colors.blueGray[800],
          },
          title: t`pageTitles.settings`,
          unmountOnBlur: true,
        }}
      />
    </Tabs>
  );
};

export default function RootLayout() {
  const [appState, dispatch] = useReducer(appReducer, initialState);

  const fetchSavedSettings = useCallback(async () => {
    const data = await getObjectData('settings');

    dispatch(setSettingsData(data));
  }, [dispatch]);

  useEffect(() => {
    fetchSavedSettings();
  }, [fetchSavedSettings]);

  useEffect(() => {
    i18n.changeLanguage(appState.language);
  }, [appState.language]);

  return (
    <AppContext.Provider value={appState}>
      <AppDispatchContext.Provider value={dispatch}>
        <I18nextProvider i18n={i18n}>
          <SafeAreaProvider>
            <NativeBaseProvider>
              <TabsWithLocalization />
            </NativeBaseProvider>
          </SafeAreaProvider>
        </I18nextProvider>
      </AppDispatchContext.Provider>
    </AppContext.Provider>
  );
}
