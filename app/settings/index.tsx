import {
  Container,
  Center,
  FormControl,
  Heading,
  Stack,
  Select,
  CheckIcon,
  Box,
  Button,
  useToast,
} from 'native-base';

import { useTranslation } from 'react-i18next';

import { useContext } from 'react';

import { storeObjectData } from '../../database';
import SafeView from '../../components/SaveView';
import { CalcPeriod, Language, Theme } from '../../types/settings';
import { AppContext, AppDispatchContext } from '../../store';

import {
  setCalcPeriod,
  setLanguage,
  setTheme,
} from '../../store/_reducer/actions';

export default function Settings() {
  const { t } = useTranslation();

  const { calcPeriod, language, theme } = useContext(AppContext);

  const dispatch = useContext(AppDispatchContext);

  const toast = useToast();

  const handleSaveSettings = () => {
    storeObjectData('settings', { calcPeriod, language, theme });

    toast.show({
      description: t`notifications.settingsSaved`,
    });
  };

  return (
    <SafeView>
      <Center>
        <Container alignItems='center'>
          <Heading bold my={4}>
            {t`settings.title`}
          </Heading>

          <Stack>
            <FormControl.Label>{t`settings.fieldsLabels.language`}</FormControl.Label>
            <Select
              selectedValue={language}
              minWidth='200'
              accessibilityLabel={t`settings.placeholders.language`}
              placeholder={t`settings.placeholders.language`}
              _selectedItem={{
                bg: 'teal.600',
                endIcon: <CheckIcon size='5' />,
              }}
              isFocusVisible={false}
              mt={1}
              onValueChange={(itemValue) => {
                dispatch(setLanguage(itemValue as Language));
              }}
            >
              <Select.Item label={t`settings.options.langEnglish`} value='en' />
              <Select.Item label={t`settings.options.langRussian`} value='ru' />
            </Select>
          </Stack>

          <Stack mt={4}>
            <FormControl.Label>{t`settings.fieldsLabels.theme`}</FormControl.Label>

            <Select
              selectedValue={theme}
              minWidth='200'
              accessibilityLabel={t`settings.placeholders.theme`}
              placeholder={t`settings.placeholders.theme`}
              _selectedItem={{
                bg: 'teal.600',
                endIcon: <CheckIcon size='5' />,
              }}
              isFocusVisible={false}
              isHovered={false}
              mt={1}
              onValueChange={(itemValue) => {
                dispatch(setTheme(itemValue as Theme));
              }}
            >
              <Select.Item label={t`settings.options.themeOS`} value='os' />
              <Select.Item label={t`settings.options.themeDark`} value='dark' />
              <Select.Item
                label={t`settings.options.themeLight`}
                value='light'
              />
            </Select>
          </Stack>

          <Stack mt={4}>
            <FormControl.Label>{t`settings.fieldsLabels.calculationPeriod`}</FormControl.Label>
            <Select
              selectedValue={calcPeriod}
              minWidth='200'
              accessibilityLabel={t`settings.placeholders.calculationPeriod`}
              placeholder={t`settings.placeholders.calculationPeriod`}
              _selectedItem={{
                bg: 'teal.600',
                endIcon: <CheckIcon size='5' />,
              }}
              mt={1}
              onValueChange={(itemValue) => {
                dispatch(setCalcPeriod(itemValue as CalcPeriod));
              }}
            >
              <Select.Item
                label={t`settings.options.calcPeriodWeek`}
                value='week'
              />
              <Select.Item
                label={t`settings.options.calcPeriodHalfOfMonth`}
                value='halfOfMonth'
              />
              <Select.Item
                label={t`settings.options.calcPeriodMonth`}
                value='month'
              />
            </Select>
          </Stack>

          <Box alignItems='center' mt={8}>
            <Button onPress={handleSaveSettings}>
              {t`settings.buttons.save`}
            </Button>
          </Box>
        </Container>
      </Center>
    </SafeView>
  );
}
