import { ScrollView } from 'native-base';
import { useEffect, useMemo, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import SafeView from '../../components/SaveView';
import { fetchWorkoutTypes } from '../../pagesUtils/fetchWorkoutTypes';
import { expandWorkoutRecords } from '../../pagesUtils/_workouts/expandWorkoutRecords';
import { fetchWorkoutRecords } from '../../pagesUtils/_workouts/fetchWorkoutRecords';
import StatisticsChart from '../../components/StatisticsChart';
import { sortWorkouts } from '../../calculations/sortWorkouts';

export default function Statistics() {
  const { t } = useTranslation();

  const isMounted = useRef(false);

  const [, setWorkoutTypes] = useState([]);
  const [workoutRecords, setWorkoutRecords] = useState([]);

  const handleRefresh = async () => {
    const newWorkoutTypes = await fetchWorkoutTypes();
    const newWorkoutRecords = await fetchWorkoutRecords();

    if (isMounted.current) {
      setWorkoutTypes(newWorkoutTypes);
      setWorkoutRecords(
        expandWorkoutRecords(newWorkoutRecords, newWorkoutTypes),
      );
    }
  };

  useEffect(() => {
    isMounted.current = true;

    handleRefresh();

    return () => {
      isMounted.current = false;
    };
  }, []);

  const sortedWorkouts = useMemo(
    () => sortWorkouts(workoutRecords),
    [workoutRecords],
  );

  return (
    <SafeView>
      <ScrollView>
        <StatisticsChart
          title={t`statistics.titles.income`}
          workoutRecords={sortedWorkouts}
          chartType='line'
          periodTypes={['year', 'month', 'halfOfMonth']}
        />

        <StatisticsChart
          title={t`statistics.titles.peopleCount`}
          workoutRecords={sortedWorkouts}
          chartType='bar'
          periodTypes={['year', 'month', 'halfOfMonth']}
        />

        <StatisticsChart
          title={t`statistics.titles.peopleCountByWorkoutType`}
          workoutRecords={sortedWorkouts}
          chartType='pie'
          periodTypes={['year', 'month', 'halfOfMonth']}
        />

        <StatisticsChart
          title={t`statistics.titles.activity`}
          workoutRecords={sortedWorkouts}
          chartType='contribution'
          periodTypes={['year']}
        />
      </ScrollView>
    </SafeView>
  );
}
