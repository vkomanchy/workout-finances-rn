import * as R from 'ramda';
import { Period, WorkoutRecordWithExpandedWorkoutType } from './types';

export const getWorkoutsForPeriod = (
  workouts: WorkoutRecordWithExpandedWorkoutType[],
  period: Period,
) =>
  R.filter(
    ({ date }: WorkoutRecordWithExpandedWorkoutType) =>
      date >= period.startDate && date <= period.endDate,
    workouts,
  );
