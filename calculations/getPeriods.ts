import * as R from 'ramda';
import { DateTime } from 'luxon';
import type { DateWithPeriodType, Period, WorkoutRecord } from './types';
import type { CalcPeriod } from '../types/settings';

export const getDates = R.curry(
  (periodType: CalcPeriod, workout: WorkoutRecord): DateWithPeriodType => ({
    date: R.path(['date'], workout),
    periodType,
  }),
);

export const createMonthPeriod = (dateTime: DateTime): Period => ({
  startDate: dateTime.startOf('month').toMillis(),
  endDate: dateTime.endOf('month').toMillis(),
});

export const createHalfMonthPeriod = (dateTime: DateTime): Period => {
  const isFirstHalf = dateTime.day <= 15;

  const halfOfMonthDate = dateTime.startOf('month').plus({ days: 14 });

  return isFirstHalf
    ? {
        startDate: dateTime.startOf('month').toMillis(),
        endDate: halfOfMonthDate
          .plus({ hours: 23, minutes: 59, seconds: 59 })
          .toMillis(),
      }
    : {
        startDate: halfOfMonthDate.plus({ days: 1 }).toMillis(),
        endDate: dateTime.endOf('month').toMillis(),
      };
};

export const createWeekPeriod = (dateTime: DateTime): Period => ({
  startDate: dateTime.startOf('week').toMillis(),
  endDate: dateTime.endOf('week').toMillis(),
});

export const createYearPeriod = (dateTime: DateTime): Period => ({
  startDate: dateTime.startOf('year').toMillis(),
  endDate: dateTime.endOf('year').toMillis(),
});

export const createPeriod = ({ date, periodType }: DateWithPeriodType) => {
  const dateTime = DateTime.fromMillis(date);

  if (periodType === 'month') {
    return createMonthPeriod(dateTime);
  }

  if (periodType === 'halfOfMonth') {
    return createHalfMonthPeriod(dateTime);
  }

  if (periodType === 'year') {
    return createYearPeriod(dateTime);
  }

  return createWeekPeriod(dateTime);
};

export const dedupePeriods = (periods: Period[]) =>
  R.values<{ [key: string]: Period }, string>(
    R.reduce(
      (acc, period) => {
        const periodKey = `${period.startDate}-${period.endDate}`;

        if (!acc[periodKey]) {
          acc[periodKey] = period;
        }

        return acc;
      },
      {},
      periods,
    ),
  );

export const createPeriodsForWorkoutsList = (
  workouts: WorkoutRecord[],
  periodType: CalcPeriod,
) => {
  const getDatesWithPredefinedPeriodType = getDates(periodType);

  const getDatesAndCreatePeriods = R.pipe(
    getDatesWithPredefinedPeriodType as () => DateWithPeriodType,
    createPeriod,
  );

  const addPeriodTypeToObject = R.assoc('periodType', periodType);

  const periods: Period[] = R.map(
    R.pipe(addPeriodTypeToObject, getDatesAndCreatePeriods),
    workouts,
  );

  return dedupePeriods(periods);
};

export const getCurrentPeriod = (periodType: CalcPeriod): Period => {
  const currentTime = DateTime.now().toMillis();

  return createPeriod({ date: currentTime, periodType });
};
