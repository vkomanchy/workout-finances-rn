import { CalcPeriod } from '../types/settings';

export interface WorkoutType {
  id: string;

  name: string;

  incomeToCashier: number;
  incomeToTrainer: number;
}

export interface WorkoutRecord {
  id: string;

  date: number;
  peopleCount: number;
}

export interface WorkoutRecordWithWorkoutTypeId extends WorkoutRecord {
  workoutType: string;
}

export interface WorkoutRecordWithExpandedWorkoutType extends WorkoutRecord {
  workoutType: WorkoutType;
}

export interface MarkedWorkoutRecord
  extends WorkoutRecordWithExpandedWorkoutType {
  markColor?: string;
}

export type CalculationFunc = (x: WorkoutRecord) => number;

export interface DateWithPeriodType {
  date: number;
  periodType: CalcPeriod;
}

export interface Period {
  startDate: number;
  endDate: number;
}

export interface PeopleCountByWorkoutType {
  name: string;
  id: string;

  peopleCount: number;
}

export interface AccPeopleCountByWorkoutType {
  [key: string]: PeopleCountByWorkoutType;
}
