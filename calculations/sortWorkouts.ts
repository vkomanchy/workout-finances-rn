import * as R from 'ramda';

import { WorkoutRecordWithExpandedWorkoutType } from './types';

export const sortByDate = R.sortBy(R.prop('date'));

export const sortWorkouts = (
  workouts: WorkoutRecordWithExpandedWorkoutType[],
) => sortByDate(workouts);
