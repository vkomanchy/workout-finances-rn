import { expect, it, describe } from 'vitest';
import { DateTime } from 'luxon';

import {
  createHalfMonthPeriod,
  createMonthPeriod,
  createPeriod,
  createPeriodsForWorkoutsList,
  createWeekPeriod,
  createYearPeriod,
  dedupePeriods,
  getDates,
} from '../getPeriods';
import { Period, WorkoutRecord } from '../types';
import { CalcPeriod } from '../../store/types';

describe('Check getDates function', () => {
  const workoutRecord = { date: 1000 } as WorkoutRecord;

  it('Should return date = 1000 and periodType - week, without curry', () => {
    expect(getDates('week', workoutRecord)).toMatchObject({
      date: 1000,
      periodType: 'week',
    });
  });

  it('Should return date = 1000 and periodType - week, with curry', () => {
    expect(getDates('week')(workoutRecord)).toMatchObject({
      date: 1000,
      periodType: 'week',
    });
  });
});

describe('Check period creation functions', () => {
  const firstHalfDate = DateTime.fromMillis(1689282000000);
  const secondHalfDate = DateTime.fromMillis(1689962821620);

  const cases: Array<{
    name: string;
    getPeriod: (data: DateTime) => Period;
    start: string;
    end: string;
    dateTime: DateTime;
  }> = [
    {
      name: 'createMonthPeriod',
      getPeriod: createMonthPeriod,
      start: '01.07.2023',
      end: '31.07.2023',
      dateTime: secondHalfDate,
    },
    {
      name: 'createHalfMonthPeriod for first half',
      getPeriod: createHalfMonthPeriod,
      start: '01.07.2023',
      end: '15.07.2023',
      dateTime: firstHalfDate,
    },
    {
      name: 'createHalfMonthPeriod for second half',
      getPeriod: createHalfMonthPeriod,
      start: '16.07.2023',
      end: '31.07.2023',
      dateTime: secondHalfDate,
    },
    {
      name: 'createWeekPeriod',
      getPeriod: createWeekPeriod,
      start: '17.07.2023',
      end: '23.07.2023',
      dateTime: secondHalfDate,
    },
    {
      name: 'createYearPeriod',
      getPeriod: createYearPeriod,
      start: '01.01.2023',
      end: '31.12.2023',
      dateTime: firstHalfDate,
    },
  ];

  it.each(cases)(
    '$name should create period with startDate - $start and endDate - $end',
    ({ getPeriod, start, end, dateTime }) => {
      const { startDate, endDate } = getPeriod(dateTime);

      expect(DateTime.fromMillis(startDate).toLocaleString()).toBe(start);
      expect(DateTime.fromMillis(endDate).toLocaleString()).toBe(end);
    },
  );
});

describe('Check createPeriod function with different periodType', () => {
  const dateTime = DateTime.fromMillis(1689962821620);

  const cases: Array<{
    start: string;
    end: string;
    dateTime: DateTime;
    periodType: CalcPeriod;
  }> = [
    {
      start: '01.07.2023',
      end: '31.07.2023',
      dateTime,
      periodType: 'month',
    },
    {
      start: '16.07.2023',
      end: '31.07.2023',
      dateTime,
      periodType: 'halfOfMonth',
    },
    {
      start: '17.07.2023',
      end: '23.07.2023',
      dateTime,
      periodType: 'week',
    },
    {
      start: '01.01.2023',
      end: '31.12.2023',
      dateTime,
      periodType: 'year',
    },
  ];

  it.each(cases)(
    'createPeriod for periodType - $periodType should create period with startDate - $start and endDate - $end',
    ({ start, end, periodType }) => {
      const { startDate, endDate } = createPeriod({
        date: dateTime.toMillis(),
        periodType,
      });

      expect(DateTime.fromMillis(startDate).toLocaleString()).toBe(start);
      expect(DateTime.fromMillis(endDate).toLocaleString()).toBe(end);
    },
  );
});

describe('Check createPeriodsForWorkoutsList', () => {
  const workouts = [
    {
      date: 1686085200000,
    },
    {
      date: 1689962821620,
    },
  ] as WorkoutRecord[];

  it('Should return predefined periods for weeks', () => {
    const predefinedPeriods = [
      {
        startDateFormatted: '05.06.2023',
        endDateFormatted: '11.06.2023',
      },
      {
        startDateFormatted: '17.07.2023',
        endDateFormatted: '23.07.2023',
      },
    ];

    expect(
      createPeriodsForWorkoutsList(workouts, 'week').map(
        ({ endDate, startDate }) => ({
          startDateFormatted: DateTime.fromMillis(startDate).toLocaleString(),
          endDateFormatted: DateTime.fromMillis(endDate).toLocaleString(),
        }),
      ),
    ).toMatchObject(predefinedPeriods);
  });

  it('Should return predefined periods for months', () => {
    const predefinedPeriods = [
      {
        startDateFormatted: '01.06.2023',
        endDateFormatted: '30.06.2023',
      },
      {
        startDateFormatted: '01.07.2023',
        endDateFormatted: '31.07.2023',
      },
    ];

    expect(
      createPeriodsForWorkoutsList(workouts, 'month').map(
        ({ endDate, startDate }) => ({
          startDateFormatted: DateTime.fromMillis(startDate).toLocaleString(),
          endDateFormatted: DateTime.fromMillis(endDate).toLocaleString(),
        }),
      ),
    ).toMatchObject(predefinedPeriods);
  });
});

describe('Test dedupePeriods', () => {
  it('Should return only one period from and remove 3 duplicates', () => {
    const duplicatedPeriods = [
      {
        startDate: 1690146000000,
        endDate: 1690750799999,
      },
      {
        startDate: 1690146000000,
        endDate: 1690750799999,
      },
      {
        startDate: 1690146000000,
        endDate: 1690750799999,
      },
      {
        startDate: 1690146000000,
        endDate: 1690750799999,
      },
    ];

    expect(dedupePeriods(duplicatedPeriods)).toHaveLength(1);
  });

  it('Should return two periods and remove 2 duplicates', () => {
    const duplicatedPeriods = [
      {
        startDate: 1690146000000,
        endDate: 1690750799999,
      },
      {
        startDate: 1690146000000,
        endDate: 1690750799999,
      },
      {
        startDate: 1690146000000,
        endDate: 1690750799999,
      },
      {
        startDate: 1690750800000,
        endDate: 1691355599999,
      },
    ];

    expect(dedupePeriods(duplicatedPeriods)).toHaveLength(2);
  });
});
