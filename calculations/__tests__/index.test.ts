import { expect, it, describe, vi } from 'vitest';
import {
  calculateTotalForWorkouts,
  countPeopleByWorkoutType,
  getOrgIncome,
  getOrgIncomeForWorkout,
  getPeopleCount,
  getTrainerIncomeForWorkout,
  multiplyWithPeopleCount,
} from '..';
import { WorkoutRecord, WorkoutRecordWithExpandedWorkoutType } from '../types';

describe('Check getPeopleCount function', () => {
  it('Should return 50 as people count', () => {
    expect(getPeopleCount({ peopleCount: 50 } as WorkoutRecord)).toBe(50);
  });
});

describe('Check multiplyWithPeopleCount', () => {
  it('Should return 50 after multiplying', () => {
    const workoutRecord = {
      peopleCount: 1,

      workoutType: {
        incomeToTrainer: 50,
      },
    } as undefined as WorkoutRecord;

    expect(
      multiplyWithPeopleCount(workoutRecord)(
        getTrainerIncomeForWorkout(workoutRecord),
      ),
    ).toBe(50);
  });
});

describe('Check getIncome functions', () => {
  it('Check that getTrainerIncomeWorkout return 70', () => {
    expect(
      getTrainerIncomeForWorkout({
        workoutType: {
          incomeToTrainer: 70,
        },
      } as unknown as WorkoutRecord),
    ).toBe(70);
  });

  it('Check that getOrgIncomeForWorkout return 50', () => {
    expect(
      getOrgIncomeForWorkout({
        workoutType: {
          incomeToCashier: 50,
        },
      } as unknown as WorkoutRecord),
    ).toBe(50);
  });

  it('Check that getOrgIncomeForWorkout return -50', () => {
    const workoutType = {
      incomeToCashier: -50,
    };

    expect(
      getOrgIncomeForWorkout({
        workoutType,
      } as unknown as WorkoutRecord),
    ).toBe(-50);
  });
});

describe('Check calculateTotalForWorkouts function', () => {
  const workoutRecords = [
    {
      peopleCount: 3,
      workoutType: {
        incomeToTrainer: 50,
        incomeToCashier: 50,
      },
    },
    {
      peopleCount: 2,
      workoutType: {
        incomeToTrainer: 25,
        incomeToCashier: 25,
      },
    },
    {
      peopleCount: 2,
      workoutType: {
        incomeToTrainer: 25,
        incomeToCashier: 25,
      },
    },
  ] as WorkoutRecordWithExpandedWorkoutType[];

  it('Check that calculateTotalForWorkouts return right sum - 100', () => {
    const getTrainerIncomeMock = vi.fn(
      (workout: WorkoutRecordWithExpandedWorkoutType) => {
        const {
          workoutType: { incomeToTrainer },
        } = workout;

        return incomeToTrainer;
      },
    );

    expect(
      calculateTotalForWorkouts(workoutRecords, getTrainerIncomeMock),
    ).toBe(100);
  });

  it('Check that getOrgIncome return right sum - 250', () => {
    expect(calculateTotalForWorkouts(workoutRecords, getOrgIncome)).toBe(250);
  });

  it('Check that getTrainerIncome return right sum - 250', () => {
    expect(calculateTotalForWorkouts(workoutRecords, getOrgIncome)).toBe(250);
  });

  /* This example use negative income in workout type.
  This mean that trainer will get money from cashier. Example: Clients buy abonements and use it for every workout.
  Trainer gets money from cashier for each workout from it.
  */
  it('Check that getOrgIncome return right  sum - 500', () => {
    const workoutRecordsWithNegativeIncome = [
      {
        peopleCount: 1,
        workoutType: {
          incomeToTrainer: 0,
          incomeToCashier: 1000,
        },
      },
      {
        peopleCount: 5,
        workoutType: {
          incomeToTrainer: 100,
          incomeToCashier: -100,
        },
      },
    ] as WorkoutRecordWithExpandedWorkoutType[];

    expect(
      calculateTotalForWorkouts(workoutRecordsWithNegativeIncome, getOrgIncome),
    ).toBe(500);
  });
});

describe('Check countPeopleByWorkoutType function', () => {
  it(`Check that countPeopleByWorkoutType wile return 
  5 people for test workout and 10 for best workout`, () => {
    const workoutRecords = [
      {
        workoutType: {
          id: 'test workout',
          name: 'test workout',
        },

        peopleCount: 4,
      },
      {
        workoutType: {
          id: 'test workout',
          name: 'test workout',
        },

        peopleCount: 1,
      },
      {
        workoutType: {
          id: 'best workout',
          name: 'best workout',
        },

        peopleCount: 7,
      },
      {
        workoutType: {
          id: 'best workout',
          name: 'best workout',
        },

        peopleCount: 3,
      },
    ] as WorkoutRecordWithExpandedWorkoutType[];

    const result = countPeopleByWorkoutType(workoutRecords);

    expect(result['test workout'].peopleCount).toBe(5);
    expect(result['best workout'].peopleCount).toBe(10);
  });
});
