import { expect, it, describe } from 'vitest';
import { sortWorkouts } from '../sortWorkouts';
import { WorkoutRecordWithExpandedWorkoutType } from '../types';

describe('Check sortWorkouts function', () => {
  it('Should return sorted by date array of workouts', () => {
    const workouts = [
      { date: 1 },
      { date: 5 },
      { date: 2 },
    ] as WorkoutRecordWithExpandedWorkoutType[];

    expect(sortWorkouts(workouts)).toMatchObject([
      { date: 1 },
      { date: 2 },
      { date: 5 },
    ]);
  });
});
