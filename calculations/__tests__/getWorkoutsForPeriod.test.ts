import { expect, it, describe } from 'vitest';
import { getWorkoutsForPeriod } from '../getWorkoutsForPeriod';
import { WorkoutRecordWithExpandedWorkoutType } from '../types';

describe('Check getDates function', () => {
  const currentPeriod = {
    startDate: 1,
    endDate: 3,
  };

  it('Should return two workouts with ids 1, 2 for period', () => {
    const workouts = [
      {
        id: 1,
        date: 2,
      },
      {
        id: 2,
        date: 1.5,
      },
      {
        id: 3,
        date: 4,
      },
    ] as unknown as WorkoutRecordWithExpandedWorkoutType[];

    const result = getWorkoutsForPeriod(workouts, currentPeriod);

    expect(result).toHaveLength(2);

    expect(result[0].id).toBe(1);
    expect(result[1].id).toBe(2);
  });
});
