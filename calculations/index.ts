import * as R from 'ramda';
import type {
  AccPeopleCountByWorkoutType,
  CalculationFunc,
  WorkoutRecord,
  WorkoutRecordWithExpandedWorkoutType,
} from './types';

export const getPeopleCount = (workout: WorkoutRecord) =>
  R.path(['peopleCount'], workout);

export const multiplyWithPeopleCount = (workout: WorkoutRecord) =>
  R.multiply(getPeopleCount(workout));

export const getTrainerIncomeForWorkout = (workout: WorkoutRecord) =>
  R.path<number>(['workoutType', 'incomeToTrainer'], workout);

export const getOrgIncomeForWorkout = (workout: WorkoutRecord) =>
  R.path(['workoutType', 'incomeToCashier'], workout);

export const getOrgIncome = (workout: WorkoutRecord) =>
  R.pipe(getOrgIncomeForWorkout, multiplyWithPeopleCount(workout))(workout);

export const getTrainerIncome = (workout: WorkoutRecord) =>
  R.pipe(getTrainerIncomeForWorkout, multiplyWithPeopleCount(workout))(workout);

export const calculateTotalForWorkouts = R.curry(
  (workouts: WorkoutRecord[], calculationFunc: CalculationFunc) => {
    const calcItems = R.map(calculationFunc, workouts);

    return R.sum(calcItems);
  },
);

export const countPeopleByWorkoutType = (
  workouts: WorkoutRecordWithExpandedWorkoutType[],
): AccPeopleCountByWorkoutType =>
  R.reduce(
    (acc, current) => {
      const currentType = R.path(['workoutType', 'id'], current);
      const peopleCount = R.path(['peopleCount'], current);

      if (acc[currentType]) {
        acc[currentType].peopleCount += peopleCount;
      } else {
        acc[currentType] = {
          name: R.path(['workoutType', 'name'], current),
          id: currentType,
          peopleCount,
        };
      }

      return acc;
    },
    {} as AccPeopleCountByWorkoutType,
    workouts,
  );
