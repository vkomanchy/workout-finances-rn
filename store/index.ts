import { createContext } from 'react';
import { AppState } from './types';

export const AppContext = createContext<AppState>(null);
export const AppDispatchContext = createContext(null);
