import { CalcPeriod } from '../types/settings';

export type Language = 'en' | 'ru';

export type Theme = 'os' | 'dark' | 'light';

export interface ActionType<T> {
  type: string;
  payload: Partial<T>;
}

export interface AppState {
  language: Language;
  theme: Theme;
  calcPeriod: CalcPeriod;
}
