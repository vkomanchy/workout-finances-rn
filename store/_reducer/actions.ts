import { CalcPeriod } from '../../types/settings';
import { Language, AppState, Theme } from '../types';

export const actionsTypes = {
  SET_LANGUAGE: 'SET_LANGUAGE',
  SET_THEME: 'SET_THEME',
  SET_CALC_PERIOD: 'SET_CALC_PERIOD',
  SET_SETTINGS_DATA: 'SET_SETTINGS_DATA',
};

export const setLanguage = (language: Language) => ({
  type: actionsTypes.SET_LANGUAGE,
  payload: {
    language,
  },
});

export const setTheme = (theme: Theme) => ({
  type: actionsTypes.SET_THEME,
  payload: {
    theme,
  },
});

export const setCalcPeriod = (calcPeriod: CalcPeriod) => ({
  type: actionsTypes.SET_CALC_PERIOD,
  payload: {
    calcPeriod,
  },
});

export const setSettingsData = (settings: AppState) => ({
  type: actionsTypes.SET_SETTINGS_DATA,
  payload: settings,
});
