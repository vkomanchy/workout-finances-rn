import { actionsTypes } from './actions';
import { ActionType, AppState } from '../types';

export const initialState: AppState = {
  language: 'en',
  theme: 'os',
  calcPeriod: 'month',
};

export const reducer = (state: AppState, action: ActionType<AppState>) => {
  switch (action.type) {
    case actionsTypes.SET_LANGUAGE: {
      return {
        ...state,
        ...action.payload,
      };
    }

    case actionsTypes.SET_THEME: {
      return {
        ...state,
        ...action.payload,
      };
    }

    case actionsTypes.SET_CALC_PERIOD: {
      return {
        ...state,
        ...action.payload,
      };
    }

    case actionsTypes.SET_SETTINGS_DATA: {
      return {
        ...state,
        ...action.payload,
      };
    }

    default:
      return state;
  }
};
