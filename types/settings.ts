export type Language = 'en' | 'ru';

export type Theme = 'os' | 'dark' | 'light';

export type CalcPeriod = 'month' | 'week' | 'halfOfMonth' | 'year';

export interface ActionType<T> {
  type: string;
  payload: Partial<T>;
}

export interface SettingsState {
  language: Language;
  theme: Theme;
  calcPeriod: CalcPeriod;
}
