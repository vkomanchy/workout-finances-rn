# workout-finances-rn

# Description
Android app for counting workouts income for trainer and cashier. Counting implemented with workout types and workout records.

## Workout types
Workout types store data about workout name and incomes to cashier and trainer. 

## Workout records
Workout records store people count, workout date and choosen workout type. People count will be multiplied with incomes to cashier and trainer from workout type.

# Installation
Install Node.js v18.16.0 and npm v9.6.7. Install packages

# Roadmap
2nd iteration
- Fix problem with text in workout type card (#19)
- Add drag-and-drop for workout types
- Add sorting for workout records
- Add formik for forms
- Add possibility to set photo on background
- Add splash screen

3d iteration
- Add calendar to change current period
- Add connection to server. Users will be divided on "clients", "trainers", "admins". workout-finances will (only optionally) become CRM-like system.
